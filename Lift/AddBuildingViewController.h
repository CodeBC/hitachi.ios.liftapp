//
//  AddBuildingViewController.h
//  Lift
//
//  Created by Zayar on 4/4/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
@interface AddBuildingViewController : UIViewController<SOAPRequestDelegate>
- (IBAction)onAddBuilding:(id)sender;
@end

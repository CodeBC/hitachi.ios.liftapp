//
//  AddBuildingViewController.m
//  Lift
//
//  Created by Zayar on 4/4/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "AddBuildingViewController.h"
#import "Building.h"
#import "SOAPRequest.h"
#import "LiftAppDelegate.h"
#import "ObjectConfig.h"

@interface AddBuildingViewController ()
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UIImageView * imgLogoView;
    IBOutlet UILabel * lblNotice;
}
@property (nonatomic, strong) IBOutlet UITextField * txtIP;
@property (nonatomic, strong) IBOutlet UITextField * txtTanetMNo;
@property (nonatomic, strong) SOAPRequest * requestBuildingAdd;
@property BOOL isTxtIPOk;
@property BOOL isTxtMNoOk;
@property BOOL hasNoNeedToCheck;
@end

@implementation AddBuildingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.navigationBar.topItem.backBarButtonItem = [[UIBarButtonItem alloc]
                                                                         initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.topItem.backBarButtonItem.tintColor = [UIColor whiteColor];
    [self setUpBackgroudViews];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    //[self.navigationController setNavigationBarHidden:NO];
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjectConfig * objConfig = [delegate.db getConfig];
    self.txtIP.text = objConfig.strBaseLink;
    self.navigationController.navigationBar.topItem.backBarButtonItem.tintColor = [UIColor whiteColor];
}

- (void)viewDidAppear:(BOOL)animated{
    self.navigationController.navigationBar.topItem.backBarButtonItem.tintColor = [UIColor whiteColor];
}


- (void) setUpBackgroudViews{
    [imgBgView setImage:[UIImage imageNamed:@"bg"]];
    [imgLogoView setImage:[UIImage imageNamed:@"logo"]];
    
    [lblNotice setText:@"In case of fire, do not use lift."];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    NSLog(@"textFieldShouldBeginEditing.....");
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    NSLog(@"textFieldDidBeginEditing.....");
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    NSLog(@"textFieldShouldEndEditing.....");
    
    /*if (textField == self.txtIP) {
        
        int ipQuads[4];
        const char *ipAddress = [self.txtIP.text cStringUsingEncoding:NSUTF8StringEncoding];
        
        sscanf(ipAddress, "%d.%d.%d.%d", &ipQuads[0], &ipQuads[1], &ipQuads[2], &ipQuads[3]);
        
        @try {
            for (int quad = 0; quad < 4; quad++) {
                if ((ipQuads[quad] < 0) || (ipQuads[quad] > 255)) {
                    NSException *ipException = [NSException
                                                exceptionWithName:@"IPNotFormattedCorrectly"
                                                reason:@"IP range is invalid"
                                                userInfo:nil];
                    self.isTxtIPOk = FALSE;
                    @throw ipException;
                }
                else {
                    NSLog(@"is Valid number!");
                    self.isTxtIPOk = TRUE;
                }
            }
        }
        @catch (NSException *exc) {
            NSLog(@"ERROR: %@", [exc reason]);
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Entry Error"
                                                           message:[exc reason]
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    else*/
    /*Bugs
    -Remove Building: Disable tabs
    -BaseURL: App crash handling*/
    
    
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == self.txtIP) {
        
        if(![self stringIsEmpty:self.txtIP.text shouldCleanWhiteSpace:YES]){
            LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate.db updateConfig:self.txtIP.text];
        }
        else{
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Entry Error"
                                                           message:@"Input data should not empty"
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    else if(textField == self.txtTanetMNo) {
        
        if(![self stringIsEmpty:self.txtTanetMNo.text shouldCleanWhiteSpace:YES]){
            NSString *regEx = @"[0-9]{7}";
            NSRange r = [self.txtTanetMNo.text rangeOfString:regEx options:NSRegularExpressionSearch];
            NSInteger cCount = [self.txtTanetMNo.text length];
            NSLog(@"count of length %d",cCount);
            if (cCount != 0) {
                if (r.location == NSNotFound) {
                    UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Entry Error"
                                                                   message:@"Input data must be 8 digits number."
                                                                  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    
                    self.isTxtMNoOk = FALSE;
                }
                else{
                    if (7<cCount && cCount<9) {
                        self.isTxtMNoOk = TRUE;
                    }
                    else{
                        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Entry Error"
                                                                       message:@"Input data must be 8 digits number."
                                                                      delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                        
                        self.isTxtMNoOk = FALSE;
                    }
                }
            }
        }
        
    }
    NSLog(@"textFieldDidEndEditing.....");
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSLog(@"shouldChangeCharactersInRange.....");
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    
    NSLog(@"textFieldShouldClear.....");
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    NSLog(@"textFieldShouldReturn.....");
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)onAddBuilding:(id)sender{
    _requestBuildingAdd = [[SOAPRequest alloc] initWithOwner:self];
    _requestBuildingAdd.processId = 1;
    //self.txtTanetMNo.text = @"98588367";
    [self textFieldDidEndEditing:self.txtTanetMNo];
    [self setEditing:FALSE];
    if (![self stringIsEmpty:self.txtTanetMNo.text shouldCleanWhiteSpace:YES] && self.isTxtMNoOk) {
        
        
            [_requestBuildingAdd syncAddBuilding:self.txtTanetMNo.text];
            [SVProgressHUD show];
//        }
//        else{
//            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Entry Error"
//                                                           message:@"Input data is not complete.Please try again!"
//                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
//        }
    
        
    }
    else{
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Entry Error"
                                                       message:@"Input data must be phone format!"
                                                      delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        self.isTxtMNoOk = FALSE;
    }
}

- (IBAction)onBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL ) stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL)cleanWhileSpace {
    
    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }
    
    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    }
    
    if (cleanWhileSpace) {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }
    
    return NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    [self.txtTanetMNo resignFirstResponder];
    [self setEditing:TRUE];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    if (processId == 1) {
        LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
        Building * building = [[Building alloc]init];
        building.status = [[dics valueForKeyPath:@"status"] intValue];
        building.strMessage = [dics valueForKeyPath:@"message"];
        if (building.status == 1) {
            building.strName = [dics valueForKeyPath:@"building_name"];
            building.strGLevel = [dics valueForKeyPath:@"building_ground"];
            building.latitude = [[dics valueForKeyPath:@"building_latitude"] doubleValue];
            building.longitude = [[dics valueForKeyPath:@"building_longitude"] doubleValue];
            building.strWifis = [dics valueForKeyPath:@"building_wifi"];
            NSLog(@"str wifi %@",building.strWifis);
            building.strIp = [dics valueForKeyPath:@"building_ip"];
            building.strHashTenantCode = [dics valueForKeyPath:@"tenant_code"];
            int status = [delegate validateBuilding:building];
//            [delegate.db updateDeselectedBuilding];
//            [delegate.db insertBuildingBy:building];
//            [SVProgressHUD showSuccessWithStatus:@"Successfully added!"];
            if (status == 1) {
                [delegate.db updateDeselectedBuilding];
                 [delegate.db insertBuildingBy:building];
                 [SVProgressHUD showSuccessWithStatus:@"Successfully added!"];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            else if (status == 2 ){
                //message = @"You are not reached the building!";
                //message = @"You are not connected building wifi!";
                UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info"
                                                               message:@"You must be within premises to use the application."
                                                              delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                [SVProgressHUD dismiss];
            }
            else if (status == 3){
                UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info"
                                                               message:@"Please connect to the building's Wifi before you can proceed."
                                                              delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                [SVProgressHUD dismiss];
            }
        }
        else if(building.status == 2 || building.status == 4 || building.status == 3) {
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Error"
                                                           message:building.strMessage
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
        }
        else if (building.status == 0){
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Sync Error"
                                                           message:@"Invalid based url or sever is down!"
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
        }
        NSLog(@"building object name %@ status %d",building.strName, building.status);
        
    }
}

- (void) onErrorLoad: (int) processId{
    NSLog(@"error process %d",processId);
    [SVProgressHUD dismiss];
    }

@end

//
//  Building.h
//  Lift
//
//  Created by Zayar on 4/3/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Building : NSObject
{
    int idx;
    NSString * strName;
    NSString * strGLevel;
    double latitude;
    double longitude;
    NSString * strIp;
    NSString * strHashTenantCode;
    NSString * strWifis;
    int status;
    NSString * strMessage;
    int isSelected;
}
@property int idx;
@property double latitude;
@property double longitude;
@property int status;
@property (nonatomic, strong) NSString * strName;
@property (nonatomic, strong) NSString * strGLevel;
@property (nonatomic, strong) NSString * strIp;
@property (nonatomic, strong) NSString * strHashTenantCode;
@property (nonatomic, strong) NSString * strWifis;
@property (nonatomic, strong) NSString * strMessage;
@property int isSelected;

-(id)initWithAttributes:(NSDictionary*)attribute;
+ (void)globalTimelinePostsWithBlock:(void (^)(Building *objBuilding, NSError *error))block andParam:(NSString *)strParam;
@end

//
//  Building.m
//  Lift
//
//  Created by Zayar on 4/3/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "Building.h"
#import "LiftAPIClient.h"
#import "AFHTTPRequestOperation.h"
/* 
 Return Data
 {
 “status”:”1”,
 “building_name”:” Hitachi Building”,
 “building_ground”:”Ground Floor”,
 “building_latitude”:” 21.004456”,
 “building_longitude”:” 17.998761”,
 “building_wifi”:” wifihotspot01, wifihotspot02”,
 “building_ip”:” 210.121.111.23”,
 “tenant_code”:” 2ec6d1d08e49a67ca44c0b0d5d8d90759107079f8123912319dca2189193040”
 }
 No record found
 {
 "status":"2",
 "message":"Not a registered tenant"
 }
 
 
 */
@implementation Building
@synthesize idx,strName,strGLevel,latitude,longitude,strIp,strHashTenantCode,strWifis,status,strMessage,isSelected;
-(id)initWithAttributes:(NSDictionary *)attribute {
    self = [super init];
    if (!self) {
        return nil;
    }
    status = [[attribute valueForKeyPath:@"status"] intValue];
    strName = [attribute valueForKeyPath:@"building_name"];
    strGLevel = [attribute valueForKeyPath:@"building_ground"];
    latitude = [[attribute valueForKeyPath:@"building_latitude"] doubleValue];
    longitude = [[attribute valueForKeyPath:@"building_longitude"] doubleValue];
    strWifis = [attribute valueForKeyPath:@"building_wifi"];
    strIp = [attribute valueForKeyPath:@"building_ip"];
    strHashTenantCode = [attribute valueForKeyPath:@"building_ip"];
    strMessage = [attribute valueForKeyPath:@"message"];
    return self;
}

+ (void)globalTimelinePostsWithBlock:(void (^)(Building *objBuilding, NSError *error))block andParam:(NSString *)strParam{
    
     NSDictionary* params = @{@"tenant_mobile":@"98588312"};
    [[LiftAPIClient sharedClient] getPath:@"/modules/external/_ext_register_tenant.php" parameters:params success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        //NSMutableArray *mutablePosts = [NSMutableArray arrayWithCapacity:[JSON count]];
        /*for (NSDictionary *attributes in JSON) {
            Post *post = [[Post alloc] initWithAttributes:attributes];
            [mutablePosts addObject:post];
        }*/
        NSDictionary *attributes = (NSDictionary *)JSON;
        Building * objBuilding = [[Building alloc] initWithAttributes:attributes];
        if (block) {
            block([NSArray arrayWithObject:objBuilding], nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (block) {
            block([NSArray array], error);
        }
    }];
    
}



@end

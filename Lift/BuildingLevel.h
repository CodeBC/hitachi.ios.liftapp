//
//  BuildingLevel.h
//  Lift
//
//  Created by Zayar on 4/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BuildingLevel : NSObject
{
    int idx;
    NSString * strName;
    NSString * strSourceLevelName;
    int sourceLevel;
    NSString * strDestinationLevelName;
    int destinationLevel;
    NSString * strType;
    NSString * strHashTenantCode;
    NSString * strLift;
}
@property int idx;
@property int sourceLevel;
@property int destinationLevel;
@property (nonatomic, strong) NSString * strName;
@property (nonatomic, strong) NSString * strSourceLevelName;
@property (nonatomic, strong) NSString * strDestinationLevelName;
@property (nonatomic, strong) NSString * strType;
@property (nonatomic, strong) NSString * strHashTenantCode;
@property (nonatomic, strong) NSString * strLift;
@end

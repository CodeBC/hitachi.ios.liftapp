//
//  DBManager.h
//  epubReader
//
//  Created by ricky on 6/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Building.h"
#import "ObjectConfig.h"

@interface DBManager : NSObject {
	
}

- (void) checkAndCreateDatabase;
- (NSInteger) insertBuildingBy:(Building *) objBuild;
- (NSMutableArray *) getAllBuilding;
- (void) deleteBuildingBy:(int) idx;
- (NSInteger) updateDeselectedBuilding;
- (NSInteger) updateSelectedBuilding:(int)idx;
- (Building *) getSelectedBuilding;
- (ObjectConfig *) getConfig;
- (void) updateConfig:(NSString *) strBaseLink;
@end

//
//  DBManager.m
//  epubReader
//
//  Created by ricky on 6/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "DBManager.h"
#import "LiftAppDelegate.h"
#import "StringTable.h"
#import "Building.h"
#import "ObjectConfig.h"

@implementation DBManager

/*** start method ***/
- (void) checkAndCreateDatabase{
    
	LiftAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString * databasePath = [documentsDir stringByAppendingPathComponent: DBNAME];
	
	NSLog(@"checking at %@", databasePath);
	BOOL success;
	
	// Create a FileManager object, we will use this to check the status
	// of the database and to copy it over if required
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	// Check if the database has already been created in the users filesystem
	success = [fileManager fileExistsAtPath:databasePath];
	
	// If the database already exists then return without doing anything
	if(success){
		NSLog(@"db found");
		
		delegate.databasePath = databasePath;
		return;
	}
	
	// If not then proceed to copy the database from the application to the users filesystem
	
	// Get the path to the database in the application package
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DBNAME];
	
	NSLog(@"copying db...");
	// Copy the database from the package to the users filesystem
	[fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];	
	
	
	delegate.databasePath = databasePath;
	
	NSLog(@"db transfered!");
}
/*** end start method ***/

/* ("idx" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "name" VARCHAR, "ground" VARCHAR, "lat" DOUBLE, "long" DOUBLE, "wifi" VARCHAR, "ip" VARCHAR, "tenant_code */

- (NSInteger) insertBuildingBy:(Building *) objBuild{
    
    LiftAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSInteger result;
	BOOL opSuccessful = FALSE;
    
    sqlite3 *database;
    sqlite3_stmt * insert_statement;
    
    if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
        const char *sql = "INSERT INTO tbl_building(name,ground,lat,long,wifi,ip,tenant_code,is_selected) VALUES(?,?,?,?,?,?,?,?)";
        
        if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
        
        sqlite3_bind_text(insert_statement, 1, [objBuild.strName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insert_statement, 2, [objBuild.strGLevel UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_double(insert_statement, 3, objBuild.latitude);
        
        sqlite3_bind_double(insert_statement, 4, objBuild.longitude);
        
		sqlite3_bind_text(insert_statement, 5, [objBuild.strWifis UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insert_statement, 6, [objBuild.strIp UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_text(insert_statement, 7, [objBuild.strHashTenantCode UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_int(insert_statement, 8, 1);
        
		int success = sqlite3_step(insert_statement);
		
		sqlite3_reset(insert_statement);
		if (success != SQLITE_ERROR) {
			NSLog(@"Building inserted!");
			result = sqlite3_last_insert_rowid(database);
			opSuccessful = TRUE;
		}
		
		sqlite3_finalize(insert_statement);
    }
    if( opSuccessful ){
		sqlite3_close(database);
		
		return result;
	}
	
	NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	sqlite3_close(database);
	
	return -1;
}

- (NSMutableArray *) getAllBuilding{
    
	LiftAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
	NSMutableArray * list = [[NSMutableArray alloc] init];
	
    
    if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        const char *sqlStatement = "SELECT * FROM tbl_building";
        
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
                Building * objBuild = [[Building alloc] init];
                
                objBuild.idx = sqlite3_column_int(compiledStatement, 0);
                
                objBuild.strName = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 1) != nil){
                     objBuild.strName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                }
                
                objBuild.strGLevel = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 2) != nil){
                    objBuild.strGLevel = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                }
                
                objBuild.latitude = sqlite3_column_double(compiledStatement, 3);
                
                objBuild.longitude = sqlite3_column_double(compiledStatement, 4);
                
                objBuild.strWifis = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 5) != nil){
                    objBuild.strWifis = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                }
                
                objBuild.strIp = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 6) != nil){
                    objBuild.strIp = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                }
                
                objBuild.strHashTenantCode = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 7) != nil){
                    objBuild.strHashTenantCode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                }
                
                objBuild.isSelected = sqlite3_column_int(compiledStatement, 8);
                
                [list addObject: objBuild];
            }
        }
        
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
	return list;
}

- (void) deleteBuildingBy:(int) idx{
    
    LiftAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    
    sqlite3 *database;
    
    sqlite3_stmt * delete_statment;
    
    if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        const char *sql = "DELETE FROM tbl_building WHERE idx=?";
        
        if (sqlite3_prepare_v2(database, sql, -1, &delete_statment, NULL) != SQLITE_OK) {
            
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            
        }
        
        sqlite3_bind_int(delete_statment, 1, idx);
        int success = sqlite3_step(delete_statment);
        
        if (success != SQLITE_DONE) {
            
            NSAssert1(0, @"Error: failed to save priority with message '%s'.", sqlite3_errmsg(database));
            
        }
        
        sqlite3_reset(delete_statment);
        
    }
    
    sqlite3_close(database);
}

- (NSInteger) updateDeselectedBuilding{
    
    LiftAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSInteger result;
	BOOL opSuccessful = FALSE;
    
    sqlite3 *database;
    sqlite3_stmt * insert_statement;
    
    if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
        const char *sql = "UPDATE tbl_building SET is_selected = 0";
        
        if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
        
		int success = sqlite3_step(insert_statement);
		
		sqlite3_reset(insert_statement);
		if (success != SQLITE_ERROR) {
			NSLog(@"building updated!");
			result = sqlite3_last_insert_rowid(database);
			opSuccessful = TRUE;
		}
		
		sqlite3_finalize(insert_statement);
    }
    if( opSuccessful ){
		sqlite3_close(database);
		
		return result;
	}
	
	NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	sqlite3_close(database);
	
	return -1;
    
}

- (NSInteger) updateSelectedBuilding:(int)idx{
    
    LiftAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSInteger result;
	BOOL opSuccessful = FALSE;
    
    sqlite3 *database;
    sqlite3_stmt * insert_statement;
    
    if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
        const char *sql = "UPDATE tbl_building SET is_selected = 1 WHERE idx = ?";
        
        if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
        sqlite3_bind_int(insert_statement, 1, idx);
		int success = sqlite3_step(insert_statement);
		
		sqlite3_reset(insert_statement);
		if (success != SQLITE_ERROR) {
			NSLog(@"building updated!");
			result = sqlite3_last_insert_rowid(database);
			opSuccessful = TRUE;
		}
		
		sqlite3_finalize(insert_statement);
    }
    if( opSuccessful ){
		sqlite3_close(database);
		
		return result;
	}
	
	NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	sqlite3_close(database);
	
	return -1;
    
}

- (Building *) getSelectedBuilding{
    
	LiftAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
    Building * objBuild=nil;
    
    if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        const char *sqlStatement = "SELECT * FROM tbl_building WHERE is_selected = 1";
        
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
                objBuild = [[Building alloc] init];
                
                objBuild.idx = sqlite3_column_int(compiledStatement, 0);
                
                objBuild.strName = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 1) != nil){
                    objBuild.strName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                }
                
                objBuild.strGLevel = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 2) != nil){
                    objBuild.strGLevel = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                }
                
                objBuild.latitude = sqlite3_column_double(compiledStatement, 3);
                
                objBuild.longitude = sqlite3_column_double(compiledStatement, 4);
                
                objBuild.strWifis = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 5) != nil){
                    objBuild.strWifis = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                }
                
                objBuild.strIp = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 6) != nil){
                    objBuild.strIp = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                }
                
                objBuild.strHashTenantCode = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 7) != nil){
                    objBuild.strHashTenantCode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                }
                
                objBuild.isSelected = sqlite3_column_int(compiledStatement, 8);
                
            }
        }
        
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
	return objBuild;
}

- (void) updateConfig:(NSString *) strBaseLink{
    
	LiftAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	
    sqlite3 *database;
	sqlite3_stmt * update_statment;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
		const char *sql = "UPDATE tbl_config SET base_link=?  WHERE idx = 1";
        
		if (sqlite3_prepare_v2(database, sql, -1, &update_statment, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
		
		sqlite3_bind_text(update_statment, 1, [strBaseLink UTF8String], -1, SQLITE_TRANSIENT);
        
        int success = sqlite3_step(update_statment);
		
		if (success != SQLITE_DONE) {
			NSAssert1(0, @"Error: failed to save priority with message '%s'.", sqlite3_errmsg(database));
		}
        
        sqlite3_reset(update_statment);
	}
	
	sqlite3_close(database);
}

- (ObjectConfig *) getConfig{
    
	LiftAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
	ObjectConfig * objConfig = nil;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		
		const char *sqlStatement = "SELECT * FROM tbl_config WHERE idx = 1";
		
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                NSLog(@"found...");
                
				objConfig = [[ObjectConfig alloc] init];
                
                objConfig.idx=sqlite3_column_int(compiledStatement, 0);
                
                objConfig.strBaseLink = @"";
				if( (char *)sqlite3_column_text(compiledStatement, 1) != nil ){
					objConfig.strBaseLink= [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
				}
                
			}
		}
		
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
	
	return objConfig;
}

@end

//
//  LFDestinationViewController.h
//  Lift
//
//  Created by Zayar on 4/6/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BuildingLevel.h"

@interface LFDestinationViewController : UIViewController
@property (nonatomic, strong) BuildingLevel * objSourceLevel;
@property (nonatomic, strong) NSMutableArray * arrDestinationLevel;
@end

//
//  LFDestinationViewController.m
//  Lift
//
//  Created by Zayar on 4/6/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "LFDestinationViewController.h"
#import "BuildingLevel.h"
#import "StringTable.h"
#import "LiftProceedViewController.h"
#import "LiftAppDelegate.h"

@interface LFDestinationViewController ()
@property (nonatomic, strong) UIPickerView *uiPickerView;
@property (nonatomic, strong) UIActionSheet *menu;
@property int selectedLevel;
@property (nonatomic, strong) IBOutlet UIButton * btnDropDown;
@property (nonatomic, strong) IBOutlet UILabel * lblSLevel;
@property (nonatomic, strong) NSMutableArray * arrBuilding;
@property BOOL isSelected;
@property BOOL isBack;
@end

@implementation LFDestinationViewController
@synthesize objSourceLevel,arrDestinationLevel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    /*UIBarButtonItem * btnProcess = [[UIBarButtonItem alloc]initWithTitle:@"Proceed" style:UIBarButtonItemStyleBordered target:self action:@selector(onOk:)];
    self.navigationItem.rightBarButtonItem = btnProcess;*/


    self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,170,0,0)];

    self.uiPickerView.delegate = self;
    self.uiPickerView.showsSelectionIndicator = YES;// note this is default to NO


    self.menu = [[UIActionSheet alloc] initWithTitle:@"Building"
                                            delegate:self
                                   cancelButtonTitle:@"Done"
                              destructiveButtonTitle:@"Cancel"
                                   otherButtonTitles:nil];
    self.selectedLevel = 0;
    self.isBack = FALSE;
}

- (void)viewWillAppear:(BOOL)animated{
    self.lblSLevel.text = objSourceLevel.strSourceLevelName;
    self.isSelected = FALSE;
    /*if ([arrDestinationLevel count]==1) {
        BuildingLevel * objLevel = [arrDestinationLevel objectAtIndex:0];
        [self.btnDropDown setTitle:objLevel.strSourceLevelName forState:normal];
        self.btnDropDown.enabled = FALSE;
        self.isSelected = TRUE;
    }
    else if([arrDestinationLevel count]==0){
        self.btnDropDown.enabled = FALSE;
        self.isSelected = TRUE;
    }
    else{
        self.btnDropDown.enabled = TRUE;
        self.isSelected = FALSE;
    }*/

}

- (void)viewDidAppear:(BOOL)animated{
    if (!self.isBack) {
        BuildingLevel * objLevel = [arrDestinationLevel objectAtIndex:0];
        [self.btnDropDown setTitle:objLevel.strSourceLevelName forState:normal];
        self.btnDropDown.enabled = TRUE;
        self.isSelected = TRUE;
        if ([arrDestinationLevel count]==1) {
            self.selectedLevel = 0;
            [self goToNext];
        }
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    if (pickerView.tag == 0) {
         BuildingLevel * objLevel = [arrDestinationLevel objectAtIndex:row];
        //ObjectCity * objCity = [arrCity objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);

        return objLevel.strSourceLevelName;

    }
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{

    return [arrDestinationLevel count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    if (pickerView.tag == 0) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedLevel = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
    }

    /*else if(pickerView.tag == 1){
     intFromIndex = row;
     }
     else if(pickerView.tag == 2){
     intToIndex = row;
     }
     else if(pickerView.tag == 3){
     intTimeIndex = row;
     }*/

}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];

    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
    }

    else if (buttonIndex == 1) {
        NSLog(@"Other Button Done Clicked and selected index %d",self.selectedLevel);

        if (self.uiPickerView.tag == 0){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            BuildingLevel * objLevel = [arrDestinationLevel objectAtIndex:self.selectedLevel];
                [self.btnDropDown setTitle:objLevel.strSourceLevelName forState:normal];
            self.isSelected = TRUE;
            [self showConfirmationDialog];
        }
    }
}

-(IBAction)onLevel:(id)sender{
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    Building * building = [delegate.db getSelectedBuilding];
    if([delegate validateBuilding:building]) {
        [self.menu setTitle:@"Level"];
        //UIButton * btn = (UIButton *)sender;
        // Add the picker
        self.uiPickerView.tag = 0;
        self.uiPickerView.delegate = self;
        [self.uiPickerView reloadAllComponents];
        [self.uiPickerView selectRow:self.selectedLevel inComponent:0 animated:YES];
        [self.menu addSubview:self.uiPickerView];
        [self.menu showInView:self.view];
        [self.menu setBounds:CGRectMake(0,0,320,600)];
    } else {
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Lift"
                                                       message:@"You must be within premises to use the application."
                                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(IBAction)onOk:(id)sender{
    if (self.isSelected) {
        /*BuildingLevel * objLevel = [arrDestinationLevel objectAtIndex:self.selectedLevel];
        BuildingLevel * objPostLevels = [[BuildingLevel alloc]init];
        objPostLevels.strSourceLevelName = objSourceLevel.strSourceLevelName;
        objPostLevels.sourceLevel = objSourceLevel.sourceLevel;
        objPostLevels.strDestinationLevelName = objLevel.strSourceLevelName;
        objPostLevels.destinationLevel = objLevel.destinationLevel;
        objPostLevels.strType = MOBILE_T_TYPE;

        LiftProceedViewController * lfProceedViewCotroller=[[LiftProceedViewController alloc] initWithNibName:@"LiftProceedViewController" bundle:[NSBundle mainBundle]];
        lfProceedViewCotroller.objBuildingLevel = objPostLevels;
        [self.navigationController pushViewController:lfProceedViewCotroller animated:YES];*/
        [self showConfirmationDialog];
    }
}

- (void) showConfirmationDialog{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:@"Proceed?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.delegate = self;
    alert.tag = 0;
    [alert show];
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex{
    if( [alertView tag] == 0 ){
		if(buttonIndex == 0 ){

		}
		else if(buttonIndex == 1) {
            NSLog(@"here it is!!");
            [self goToNext];
		}
	}

}

- (void)goToNext{
    self.isBack = TRUE;
    BuildingLevel * objLevel = [arrDestinationLevel objectAtIndex:self.selectedLevel];
    BuildingLevel * objPostLevels = [[BuildingLevel alloc]init];
    objPostLevels.strSourceLevelName = objSourceLevel.strSourceLevelName;
    objPostLevels.sourceLevel = objSourceLevel.sourceLevel;
    objPostLevels.strDestinationLevelName = objLevel.strSourceLevelName;
    objPostLevels.destinationLevel = [objLevel.strDestinationLevelName intValue];
    NSLog(@"Go to destination %d", objPostLevels.destinationLevel);
    objPostLevels.strType = MOBILE_T_TYPE;

    LiftProceedViewController * lfProceedViewCotroller=[[LiftProceedViewController alloc] initWithNibName:@"LiftProceedViewController" bundle:[NSBundle mainBundle]];
    lfProceedViewCotroller.objBuildingLevel = objPostLevels;
    [self.navigationController pushViewController:lfProceedViewCotroller animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

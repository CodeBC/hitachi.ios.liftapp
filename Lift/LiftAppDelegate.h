//
//  LiftAppDelegate.h
//  Lift
//
//  Created by Zayar on 4/2/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "DBManager.h"

@interface LiftAppDelegate : UIResponder <UIApplicationDelegate>
{
    CLLocationManager *locationManager;
    CLLocationCoordinate2D currentGeo;
    
    double currentLat;
    double currentLng;
}
@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) NSString * databasePath;
@property (nonatomic, strong) DBManager * db;
@property (nonatomic, strong) NSString * strSelectedIP;
@property double currentLat;
@property double currentLng;
- (BOOL)isNearTheBuilding:(double)lat andLong:(double)longitude andMeter:(double)distance;
- (BOOL)compareNetworkSSID:(NSString *)buildingWifiSSID;
- (void) disableTheTabs:(UITabBarController *)tabBar;
- (int)validateBuilding:(Building *)objBuilding;
@end

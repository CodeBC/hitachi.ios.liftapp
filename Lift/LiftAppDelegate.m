//
//  LiftAppDelegate.m
//  Lift
//
//  Created by Zayar on 4/2/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "LiftAppDelegate.h"
#import <MapKit/MapKit.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import "Building.h"
#import "Utility.h"
#import <Crashlytics/Crashlytics.h>
@implementation LiftAppDelegate
@synthesize strSelectedIP,db,databasePath,currentLat,currentLng;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Crashlytics startWithAPIKey:@"be5731fac922d29f3b102c5b27f6f4f36e8991f0"];
    // Override point for customization after application launch.
    [self locationManager];
    self.db = [[DBManager alloc] init];
	[self.db checkAndCreateDatabase];
    [self currentWifiSSID];
    strSelectedIP = @"";
    
    
    #if IS_DEMO
    NSMutableArray * arr = [self.db getAllBuilding];
    if ([arr count] == 0) {
        Building * objBuild= [[Building alloc] init];
        objBuild.strName = @"Hitachi Demo";
        objBuild.strGLevel = @"B1,1,2,3,4,5,6";
        objBuild.latitude = 16.80717;
        objBuild.longitude = 96.132875;
        objBuild.strWifis = @"NEXLABS";
        objBuild.strIp = @"192.168.88.250";
        
        [self.db insertBuildingBy:objBuild];
    }
    #endif
    
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        //self.window.tintColor = [UIColor colorWithHexString:@"55afb4"];
        //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        self.window.tintColor = [UIColor whiteColor];
        //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    return YES;
}

- (void)locationManager {
	
	/*if (locationManager != nil) {
     //return locationManager;
     }*/
	
	locationManager = [[CLLocationManager alloc] init];
	[locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
	[locationManager setDelegate:self];
    NSLog(@"here to load place");
	[locationManager startUpdatingLocation];
	//return locationManager;
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
	
	currentGeo = [newLocation coordinate];
	
	//currentGeo.latitude = 16.850096f;
	//currentGeo.longitude = 96.129223f;
    
    //currentGeo.latitude = 22.211539;
    //currentGeo.longitude =96.059560;
	
//	NSLog(@"lat: %+.6f, lng: %+.6f", currentGeo.latitude, currentGeo.longitude);
    self.currentLat = currentGeo.latitude;
    self.currentLng = currentGeo.longitude;
	
	/*
     if( isBackground && notShown ){
     notShown = FALSE;
     
     UILocalNotification *localNotif = [[UILocalNotification alloc] init];
     if (localNotif) {
     localNotif.alertBody = @"You are around Karaweik and they are running a promotion. Do you wanna check their discount coupon?";
     localNotif.alertAction = NSLocalizedString(@"YES", nil);
     localNotif.soundName = @"alarmsound.caf";
     localNotif.applicationIconBadgeNumber = 0;
     [bgInstance presentLocalNotificationNow:localNotif];
     [localNotif release];
     }
     }
     */
}

- (void)loadingLatLong{
    [NSTimer scheduledTimerWithTimeInterval:3.0
                                     target:self
                                   selector:@selector(stopLoadingLatLong)
                                   userInfo:nil
                                    repeats:NO];
    [SVProgressHUD show];
}

- (void)stopLoadingLatLong{
    if (currentLat == 0.0 && currentLng == 0.0) {
        [self loadingLatLong];
    }
    else [SVProgressHUD dismiss];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    NSLog(@"location error %@",error);
}

- (BOOL)isNearTheBuilding:(double)lat andLong:(double)longitude andMeter:(double)distance{
    CLLocation *loc1 = [[CLLocation alloc]  initWithLatitude:lat longitude:longitude];
    CLLocation *loc2 = [[CLLocation alloc]  initWithLatitude:self.currentLat longitude:self.currentLng];
    //CLLocation *loc2 = [[CLLocation alloc]  initWithLatitude:lat longitude:longitude];
    double dMeters = [loc1 distanceFromLocation:loc2];
    NSLog(@"d meters %f and building lat: %f and long: %f and current lat:%f and long:%f",dMeters,lat,longitude,self.currentLat,self.currentLat);
    if (dMeters > distance) return FALSE;
    else return TRUE;
}

- (BOOL)compareNetworkSSID:(NSString *)buildingWifiSSID{
    NSString * phSSID = [self currentWifiSSID];
    if ([phSSID isEqualToString:buildingWifiSSID])
        return TRUE;
    else return FALSE;
}

- (NSString *)currentWifiSSID {
    // Does not work on the simulator.
    NSString *ssid = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info[@"SSID"]) {
            ssid = info[@"SSID"];
        }
    }
    NSLog(@"SSID %@",ssid);
    return ssid;
}

- (int)validateBuilding:(Building *)objBuilding{
    int status = 0;
    NSString * message = @"";
    BOOL isNear = FALSE;
    BOOL isInUnderSSID = TRUE;
    if ([self isNearTheBuilding:objBuilding.latitude andLong:objBuilding.longitude andMeter:1000]) {
        isNear = TRUE;
    }
    else {
        isNear = FALSE;
        status = 2;
        //message = @"You are not reached the building!";
    }

    if (objBuilding.strWifis != nil && ![objBuilding.strWifis isEqualToString:@""]) {
        NSArray * commands = nil;
        if( [objBuilding.strWifis rangeOfString:@","].location != NSNotFound ){
            
            commands = [objBuilding.strWifis componentsSeparatedByString:@","];
            
            for(NSString * strWifiName in commands){

                NSLog(@"strWifiname %@",strWifiName);
                if ([self compareNetworkSSID:strWifiName]) {
                    isInUnderSSID = TRUE;
                    break;
                }
                else {
                    isInUnderSSID = FALSE;
                    status = 3;
                    //message = @"You are not connected building wifi!";
                }
            }
        }
        else{
            NSLog(@"strWifiname %@",objBuilding.strWifis);
            if ([self compareNetworkSSID:objBuilding.strWifis]){
                isInUnderSSID = TRUE;
            }
            else {
                isInUnderSSID = FALSE;
                status = 3;
            }
        }
    }
    
    
    /*if (isInUnderSSID) {
        status = 1;
        message = @"Valid building!";
    }
    else if(isNear) {
        status = 4;
        message = @"Near the building!";
    }
    if (!isInUnderSSID) {
        if (isNear) {
            status = 1;
            message = @"Valid building!";
        }
    }
    else{
        if (isNear && isInUnderSSID) {
            status = 1;
            message = @"Valid building!";
        }
    }
     */
    if (isNear) {
        status = 1;
        message = @"Valid building!";
    } else {
        //status = 2;
        //message = @"Invalid building!";
        if (isInUnderSSID) {
            status = 1;
            message = @"Valid building!";
        }
        else{
            status = 2;
            message = @"Invalid building!";
        }
    }
    NSLog(@"message sucessful %@",message);
    return status;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void) disableTheTabs:(UITabBarController *)tabBar{
    Building * objBuilding = [self.db getSelectedBuilding];
    if (objBuilding == nil) {
        //tabBar.tabBarItem.
        [[[[tabBar tabBar]items]objectAtIndex:0]setEnabled:FALSE];
        [[[[tabBar tabBar]items]objectAtIndex:1]setEnabled:FALSE];
    }
    else{
        [[[[tabBar tabBar]items]objectAtIndex:0]setEnabled:TRUE];
        [[[[tabBar tabBar]items]objectAtIndex:1]setEnabled:TRUE];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    //BOOL isNear = [self isNearTheBuilding:16.794897 andLong:96.176762 andMeter:300.00];
    //NSLog(@"yep is near %d",isNear);
    //NSLog(@"SSID info %@",[self currentWifiSSID]);
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

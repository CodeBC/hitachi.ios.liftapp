//
//  LiftFirstViewController.h
//  Lift
//
//  Created by Zayar on 4/2/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
@interface LiftFirstViewController : UIViewController<SOAPRequestDelegate>
-(IBAction)onLevel:(id)sender;
- (IBAction)onSetting:(id)sender;
- (void) disableTheDestination:(BOOL) show;

@end

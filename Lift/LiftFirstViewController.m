//
//  LiftFirstViewController.m
//  Lift
//
//  Created by Zayar on 4/2/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "LiftFirstViewController.h"
#import "Building.h"
#import "LiftAppDelegate.h"
#import "SOAPRequest.h"
#import "BuildingLevel.h"
#import "LFDestinationViewController.h"
#import "StringTable.h"
#import "JSONKit.h"
#import "ProceedCardView.h"
#import "Utility.h"
#import "FXBlurView.h"
@interface LiftFirstViewController ()
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UIImageView * imgLogoView;
    IBOutlet UILabel * lblNotice;
    IBOutlet UILabel * lblSelectLocation;
    IBOutlet UILabel * lblSelectDesLocation;
    IBOutlet UILabel * lblProcced;
    IBOutlet UILabel * lblGroup;

    IBOutlet UIImageView * imgBtnLineBg;
    ProceedCardView * proceedCardView;
    FXBlurView * fxBlurView;
    NSMutableArray * arrGroupCall;
    IBOutlet UILabel *lblBuildingName;
}
@property (nonatomic, strong) UIPickerView *uiPickerView;
@property (nonatomic, strong) UIActionSheet *menu;
@property int selectedLevel;
@property int selectedDestinationLevel;
@property int selectedGroup;
@property (nonatomic, strong) IBOutlet UIButton * btnCurrent;
@property (nonatomic, strong) IBOutlet UIButton * btnDestination;
@property (nonatomic, strong) IBOutlet UIButton * btnProceed;
@property (nonatomic, strong) IBOutlet UIButton * btnGroupCall;
@property (nonatomic, strong) IBOutlet UIButton * btnReserve;
@property (nonatomic, strong) IBOutlet UIButton * btnSetting;
@property (strong, nonatomic) IBOutlet UIButton *btnRefresh;
@property (nonatomic, strong) NSMutableArray * arrBuildingLevel;
@property (nonatomic, strong) NSMutableArray * arrDestinationLevel;
@property (nonatomic, strong) SOAPRequest * requestBuildingAccess;
@property (nonatomic, strong) SOAPRequest * requestLift;
@property BOOL isSelected;
@property BOOL isDestinationSelected;
@property BOOL isGroupSelected;
@property (nonatomic, strong) BuildingLevel * objSelectedLevel;
@property (nonatomic, strong) BuildingLevel * objDestinationLevel;
@property (nonatomic, strong) BuildingLevel * objResultLevel;
@end

@implementation LiftFirstViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,170,0,0)];

    self.uiPickerView.delegate = self;
    self.uiPickerView.showsSelectionIndicator = YES;// note this is default to NO


    self.menu = [[UIActionSheet alloc] initWithTitle:@"Building"
                                            delegate:self
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:nil];
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDone.frame = CGRectMake(80,240, 170, 30);
    [btnDone addTarget:self action:@selector(onActionDone:) forControlEvents:UIControlEventTouchUpInside];
    [btnDone setTitle:@"Done" forState:normal];
    [self.menu addSubview:btnDone];

    CGRect newFrame = btnDone.frame;
    newFrame.origin.y  +=  30+5;
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCancel.frame = newFrame;
    [btnCancel addTarget:self action:@selector(onActionCancel:) forControlEvents:UIControlEventTouchUpInside];
    [btnCancel setTitle:@"Cancel" forState:normal];
    [self.menu addSubview:btnCancel];
    btnDone.titleLabel.textColor = [UIColor colorWithHexString:@"63ab93"];
    btnCancel.titleLabel.textColor = [UIColor colorWithHexString:@"63ab93"];

    [Utility makeBorder:btnDone andWidth:1 andColor:[UIColor colorWithHexString:@"63ab93"]];
    [Utility makeBorder:btnCancel andWidth:1 andColor:[UIColor colorWithHexString:@"63ab93"]];
    [Utility makeCornerRadius:btnDone andRadius:5];
    [Utility makeCornerRadius:btnCancel andRadius:5];

    CGRect pickerRect = self.uiPickerView.bounds;
    pickerRect.origin.y = 35;
    self.uiPickerView.frame = pickerRect;

    self.selectedLevel = 0;
    self.arrBuildingLevel = [[NSMutableArray alloc]init];
    self.arrDestinationLevel = [[NSMutableArray alloc] init];
    [self setUpBackgroudViews];
    [self setButtonsViews];

    if (proceedCardView == nil) {
        proceedCardView = [[ProceedCardView alloc] initWithFrame:CGRectMake(15, 568, 285, 276)];
    }
    proceedCardView.hidden = YES;
    proceedCardView.owner = self;
    [proceedCardView setUpViewsForSuccess];
    [Utility makeCornerRadius:proceedCardView andRadius:10];
    [self.view addSubview:proceedCardView];

    [self hardCodeGroupCall];

    if (!self.objResultLevel) {
        self.objResultLevel = [[BuildingLevel alloc] init];
    }
    UIBarButtonItem * btnRes = [[UIBarButtonItem alloc] initWithTitle:@"Reverse" style:UIBarButtonItemStylePlain target:self action:@selector(onReverse:)];
    self.navigationItem.rightBarButtonItem = btnRes;

    lblBuildingName.font = [UIFont systemFontOfSize:16];
    lblBuildingName.textColor = [UIColor whiteColor];
}

- (void) positionToLeft:(BOOL)left{
    int subtract_height = 0;
    int reduce_width = 0;
    int reduce_height = 0;
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        subtract_height = 0;
        reduce_width = 0;
        reduce_height = 0;
        if ([Utility isLessOSVersion:@"7.0"]) {
            subtract_height = 30;
        }
    }
    else{
        if ([Utility isLessOSVersion:@"7.0"]) {
            subtract_height = 70;
        }
        else{
            subtract_height = 50;
            reduce_width = 5;
            reduce_height = 2;
        }

    }
    [self screenviewAdjust];

    if (left) {
        [imgBtnLineBg setImage:[UIImage imageNamed:@"bg_btn_line"]];
        /*[imgBtnLineBg setFrame:CGRectMake(0, 0, 0, 0)];
         [lblSelectBuilding setFrame:CGRectMake(0, 0, 0, 0)];
         [lblAddBuilding setFrame:CGRectMake(0, 0, 0, 0)];
         [lblRemoveBuilding setFrame:CGRectMake(0, 0, 0, 0)];
         [lblChangeBuilding setFrame:CGRectMake(0, 0, 0, 0)];
         [btnSelectBuilding setFrame:CGRectMake(0, 0, 0, 0)];
         [btnAddBuilding setFrame:CGRectMake(0, 0, 0, 0)];
         [btnRemoveBuilding setFrame:CGRectMake(0, 0, 0, 0)];
         [btnChangeBuilding setFrame:CGRectMake(0, 0, 0, 0)];*/
        //[imgBtnLineBg setFrame:CGRectMake(192,111,128,386)];
        [imgBtnLineBg setFrame:CGRectMake(0, 0, 0, 0)];
        imgBtnLineBg.alpha = 0.5;
        [lblSelectLocation setFrame:CGRectMake(95-60,144-subtract_height,175-reduce_width,21-reduce_height)];
        [lblSelectDesLocation setFrame:CGRectMake(-30,234-subtract_height,189-reduce_width,21)];
        [lblProcced setFrame:CGRectMake(-55,353-subtract_height,189-reduce_width,21-reduce_height)];
        [lblGroup setFrame:CGRectMake(27-50,445-subtract_height,250-reduce_width,21-reduce_height)];
        [self.btnCurrent setFrame:CGRectMake(1+220,116-subtract_height,86-reduce_width,86-reduce_width)];
        [self.btnDestination setFrame:CGRectMake(61+110,206-subtract_height,86-reduce_width,86-reduce_width)];
        [self.btnProceed setFrame:CGRectMake(52+90,312-subtract_height,103-reduce_width,103-reduce_width)];
        [self.btnGroupCall setFrame:CGRectMake(9+225,421-subtract_height,70-reduce_width,70-reduce_width)];
        lblSelectLocation.textAlignment = NSTextAlignmentLeft;
        lblSelectDesLocation.textAlignment = NSTextAlignmentLeft;
        lblProcced.textAlignment = NSTextAlignmentLeft;
        lblGroup.textAlignment = NSTextAlignmentLeft;

        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            imgBtnLineBg.alpha = 1;
            [imgBtnLineBg setFrame:CGRectMake(0,111-subtract_height,128,386)];
            [lblSelectLocation setFrame:CGRectMake(95,144-subtract_height,175-reduce_width,21-reduce_height)];
            [lblSelectDesLocation setFrame:CGRectMake(154,234-subtract_height,189-reduce_width,21-reduce_height)];
            [lblProcced setFrame:CGRectMake(163,353-subtract_height,189-reduce_width,21-reduce_height)];
            [lblGroup setFrame:CGRectMake(27+60,445-subtract_height,250-reduce_width,21-reduce_height)];
            [self.btnCurrent setFrame:CGRectMake(1,116-subtract_height,86-reduce_width,86)];
            [self.btnDestination setFrame:CGRectMake(61,206-subtract_height,86-reduce_width,86)];
            [self.btnProceed setFrame:CGRectMake(52,312-subtract_height,103-reduce_width,103)];
            [self.btnGroupCall setFrame:CGRectMake(9,421-subtract_height,70-reduce_width,70)];
        } completion:^(BOOL finished) {
        }];
    }
    else{
        [imgBtnLineBg setImage:[UIImage imageNamed:@"bg_btn_line_right"]];
        [imgBtnLineBg setFrame:CGRectMake(320, 0, 0, 0)];
        imgBtnLineBg.alpha = 0.5;
        //[imgBtnLineBg setFrame:CGRectMake(0,111,128,386)];
        [lblSelectLocation setFrame:CGRectMake(95,144-subtract_height,175-reduce_width,21-reduce_height)];
        [lblSelectDesLocation setFrame:CGRectMake(154,234-subtract_height,189-reduce_width,21-reduce_height)];
        [lblProcced setFrame:CGRectMake(163,353-subtract_height,189-reduce_width,21-reduce_height)];
        [lblGroup setFrame:CGRectMake(27,445-subtract_height,250-reduce_width,21-reduce_height)];
        [self.btnCurrent setFrame:CGRectMake(1,116-subtract_height,86-reduce_width,86-reduce_width)];
        [self.btnDestination setFrame:CGRectMake(61,206-subtract_height,86-reduce_width,86-reduce_width)];
        [self.btnProceed setFrame:CGRectMake(52,312-subtract_height,103-reduce_width,103-reduce_width)];
        [self.btnGroupCall setFrame:CGRectMake(9,421-subtract_height,70-reduce_width,70-reduce_width)];
        lblSelectLocation.textAlignment = NSTextAlignmentRight  ;
        lblSelectDesLocation.textAlignment = NSTextAlignmentRight;
        lblProcced.textAlignment = NSTextAlignmentRight;
        lblGroup.textAlignment = NSTextAlignmentRight;

        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            imgBtnLineBg.alpha = 1;
            [imgBtnLineBg setFrame:CGRectMake(192,111-subtract_height,128,386)];
            [lblSelectLocation setFrame:CGRectMake(95-60,144-subtract_height,175-reduce_width,21-reduce_height)];
            [lblSelectDesLocation setFrame:CGRectMake(-25,234-subtract_height,189-reduce_width,21-reduce_height)];
            [lblProcced setFrame:CGRectMake(-55,353-subtract_height,189-reduce_width,21-reduce_height)];
            [lblGroup setFrame:CGRectMake(27-50,445-subtract_height,250-reduce_width,21-reduce_height)];
            [self.btnCurrent setFrame:CGRectMake(1+220,116-subtract_height,86-reduce_width,86-reduce_width)];
            [self.btnDestination setFrame:CGRectMake(61+110,206-subtract_height,86-reduce_width,86-reduce_width)];
            [self.btnProceed setFrame:CGRectMake(52+90,312-subtract_height,103-reduce_width,103-reduce_width)];
            [self.btnGroupCall setFrame:CGRectMake(9+225,421-subtract_height,70-reduce_width,70-reduce_width)];
        } completion:^(BOOL finished) {

        }];
    }
}

- (void) screenviewAdjust{
    int subtract_height = 75;
    CGRect btnReverseframe = self.btnReserve.frame;
    CGRect lblNoticeFrame = lblNotice.frame;
    CGRect btnSettingFrame = self.btnSetting.frame;
    CGRect imgBgViewFrame = imgBgView.frame;
    CGRect btnRefreshFrame = self.btnRefresh.frame;
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    NSLog(@"height btn reserve height %f lbl notice height %f btnSettingFrame height %f",btnReverseframe.origin.y,lblNoticeFrame.origin.y,btnSettingFrame.origin.y);
    if (screenBounds.size.height == 568) {
        if ([Utility isLessOSVersion:@"7.0"]) {
            subtract_height = 10;
            btnReverseframe.origin.y = 495 - subtract_height;
            self.btnReserve.frame = btnReverseframe;

            lblNoticeFrame.origin.y  = 531 - (subtract_height + 5);
            lblNotice.frame = lblNoticeFrame;

            btnSettingFrame.origin.y  = 528- (subtract_height + 10);
            self.btnSetting.frame = btnSettingFrame;

            btnRefreshFrame.origin.y = 289;
            self.btnRefresh.frame = btnRefreshFrame;

            //lblBuildingFrame.origin.y =
        }
    }
    else{
        if ([Utility isLessOSVersion:@"7.0"]) {
            subtract_height = 95;
            btnReverseframe.origin.y = 495 - subtract_height;
            self.btnReserve.frame = btnReverseframe;

            lblNoticeFrame.origin.y  = 531 - (subtract_height + 5);
            lblNotice.frame = lblNoticeFrame;

            btnSettingFrame.origin.y  = 528- (subtract_height + 10);
            self.btnSetting.frame = btnSettingFrame;

            imgBgViewFrame.origin.y = -40;
            imgBgView.frame = imgBgViewFrame;

            btnRefreshFrame.origin.y = 289-40;
            self.btnRefresh.frame = btnRefreshFrame;
        }
        else{
            btnReverseframe.origin.y = 495 - subtract_height;
            self.btnReserve.frame = btnReverseframe;

            lblNoticeFrame.origin.y  = 531 - (subtract_height + 5);
            lblNotice.frame = lblNoticeFrame;

            btnSettingFrame.origin.y  = 528- (subtract_height + 10);
            self.btnSetting.frame = btnSettingFrame;

            btnRefreshFrame.origin.y = 289-40;
            self.btnRefresh.frame = btnRefreshFrame;
        }

    }
}

- (void) hardCodeGroupCall{
    if (!arrGroupCall) arrGroupCall = [[NSMutableArray alloc] init];
    NSArray * commands = nil;
    if( [GROUP_CALL_STR rangeOfString:@","].location != NSNotFound ){

        commands = [GROUP_CALL_STR componentsSeparatedByString:@","];
        int i = 2;
        for(NSString * strGroup in commands){

            [arrGroupCall addObject:strGroup];
            i ++;
        }
    }
    else
        [arrGroupCall addObject:GROUP_CALL_STR];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setUpBackgroudViews{
    [imgBgView setImage:[UIImage imageNamed:@"bg"]];
    [imgLogoView setImage:[UIImage imageNamed:@"logo"]];

    [lblNotice setText:@"In case of fire, do not use lift."];
}

- (void) viewWillAppear:(BOOL)animated{
    NSLog(@"First View");
    [self.navigationController setNavigationBarHidden:YES];
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate disableTheTabs:self.tabBarController];
    Building * building = [delegate.db getSelectedBuilding];
    lblBuildingName.text = building.strName;
    #if IS_DEMO
    NSString * tenant_level = @"B1,1,2,3,4,5,6";
    if (![self stringIsEmpty:tenant_level shouldCleanWhiteSpace:YES]) {
        NSArray * commands = nil;
        if( [tenant_level rangeOfString:@","].location != NSNotFound ){

            commands = [tenant_level componentsSeparatedByString:@","];
            int i = 2;
            for(NSString * strLevelName in commands){
                BuildingLevel * objBLevel = [[BuildingLevel alloc]init];
                objBLevel.idx = i;
                objBLevel.sourceLevel = [strLevelName intValue];
                objBLevel.strDestinationLevelName = strLevelName;
                objBLevel.strSourceLevelName = [NSString stringWithFormat:@"Level %@",strLevelName];
                //NSLog(@"str name %@ and count %d",strCateName,count);
                [self.arrBuildingLevel addObject:objBLevel];
                [self.arrDestinationLevel addObject:objBLevel];
                i ++;
            }
        }
        else {
            BuildingLevel * objBLevel = [[BuildingLevel alloc]init];
            objBLevel.idx = 2;
            objBLevel.sourceLevel = [tenant_level intValue];
            objBLevel.strDestinationLevelName = tenant_level;
            objBLevel.strSourceLevelName = [NSString stringWithFormat:@"Level %@",tenant_level];
            NSLog(@"str name %@ and count %d",tenant_level,objBLevel.idx);
            [self.arrBuildingLevel addObject:objBLevel];
            [self.arrDestinationLevel addObject:objBLevel];
        }

    }
    #else
    if (building != nil) {
        if (delegate.currentLat == 0.0 && delegate.currentLng == 0.0) {
            [self loadingLatLong];
        }
        else {
            NSLog(@"Validating the building");
            [self validateBuilding:building];
        }

        [self enableTheDestination:NO];
    }
    else{
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info"
                                                       message:@"Please select or add a building before you can proceed."
                                                      delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        self.tabBarController.selectedIndex = 2;
    }
    self.isSelected = FALSE;
        #endif

    NSInteger isLeft = [[NSUserDefaults standardUserDefaults] integerForKey:@"Position"];
    if (isLeft == 0) {
        [self positionToLeft:NO];
    }
    else if(isLeft == 1){
        [self positionToLeft:YES];
    }
    NSLog(@"Building level %@", self.arrBuildingLevel);
    if ([self.arrBuildingLevel count]>0) {
        self.selectedLevel = 0;
        BuildingLevel * objLevel = [self.arrBuildingLevel objectAtIndex:self.selectedLevel];
        self.objSelectedLevel = objLevel;
        NSString * strSourceName= @"";
        /*if (objLevel.sourceLevel == 0) {
            strSourceName = @"G";
        }
        else{
            strSourceName = [NSString stringWithFormat:@"%d",objLevel.sourceLevel];
        }*/
        if ([Utility stringIsEmpty:objLevel.strSourceLevelName shouldCleanWhiteSpace:YES]) {
            strSourceName = @"";
        }
        else{
            NSRange range = [objLevel.strSourceLevelName rangeOfString:@" " options:NSBackwardsSearch];
            NSString *result = [objLevel.strSourceLevelName substringFromIndex:range.location+1];
            strSourceName = result;
        }

        [self.btnCurrent setTitle:strSourceName forState:normal];

        self.isSelected = TRUE;

        //[self showConfirmationDialog];
        [self enableTheDestination:YES];

        self.arrDestinationLevel = self.arrBuildingLevel;
        [self.arrDestinationLevel removeObjectAtIndex:self.selectedLevel];
        NSLog(@"%@", self.arrDestinationLevel);
        [self.uiPickerView reloadAllComponents];
        if ([self.arrDestinationLevel count] >1) {
            BuildingLevel * objLevel;
            if (self.selectedLevel+1 <[self.arrDestinationLevel count]) {
                self.selectedDestinationLevel = self.selectedLevel+1;
                objLevel = [self.arrDestinationLevel objectAtIndex:self.selectedDestinationLevel];
            }

            self.objDestinationLevel = objLevel;
            NSString * strSourceName;
           /* if (objLevel.sourceLevel == 0) {
                strSourceName = @"G";
            }
            else{
                strSourceName = [NSString stringWithFormat:@"%d",objLevel.sourceLevel];
            }*/
            if ([Utility stringIsEmpty:objLevel.strSourceLevelName shouldCleanWhiteSpace:YES]) {
                strSourceName = @"";
            }
            else{
                NSRange range = [objLevel.strSourceLevelName rangeOfString:@" " options:NSBackwardsSearch];
                NSString *result = [objLevel.strSourceLevelName substringFromIndex:range.location+1];
                strSourceName = result;
            }

            [self.btnDestination setTitle:strSourceName forState:normal];
            self.isDestinationSelected = TRUE;
        }
    }
    NSLog(@"Destination %@", self.arrDestinationLevel);
    self.isGroupSelected = 0;
    NSString * str = [arrGroupCall objectAtIndex:self.selectedGroup];
    [self.btnGroupCall setTitle:str forState:normal];
    self.isGroupSelected = TRUE;

}

- (void) enableTheDestination:(BOOL) show{
    if (show) {
        self.btnDestination.enabled = TRUE;
    }
    else{
        self.btnDestination.enabled = FALSE;
    }
}

- (void) setButtonsViews{
    [imgBtnLineBg setImage:[UIImage imageNamed:@"bg_btn_line"]];
}

- (void) loadingLatLong{
    [NSTimer scheduledTimerWithTimeInterval:2.0
                                     target:self
                                   selector:@selector(stopLoadingLatLong)
                                   userInfo:nil
                                    repeats:NO];
    [SVProgressHUD show];
}

- (void) stopLoadingLatLong{
     LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (delegate.currentLat == 0.0 && delegate.currentLng == 0.0) {
        [self loadingLatLong];
    }
    else {
        [SVProgressHUD dismiss];
        Building * building = [delegate.db getSelectedBuilding];
        [self validateBuilding:building];
        }
}

- (void) validateBuilding:(Building *)building{
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    int status = [delegate validateBuilding:building];

    if (status == 1) {

        _requestBuildingAccess = [[SOAPRequest alloc] initWithOwner:self];
        _requestBuildingAccess.processId = 2;
        [_requestBuildingAccess syncAccessBuilding:building.strHashTenantCode];
        [SVProgressHUD show];
        /*NSString * strPath = [[NSBundle mainBundle]pathForResource:@"tanent_level" ofType:@"json"];
        NSData * jsonData= [NSData dataWithContentsOfFile:strPath];
        NSDictionary *dics;
        JSONDecoder* decoder = [[JSONDecoder alloc]
                            initWithParseOptions:JKParseOptionNone];
        //NSMutableArray * arrTemp = [[NSMutableArray alloc]init];
        dics = [decoder objectWithData:jsonData];
        int status = [[dics valueForKeyPath:@"status"] intValue];
        //NSString * message = [dics valueForKeyPath:@"message"];
        if (status == 1) {
            [self.arrBuildingLevel removeAllObjects];
            BuildingLevel * objBLevel = [[BuildingLevel alloc]init];
            objBLevel.idx = 1;
            objBLevel.sourceLevel = 0;
            objBLevel.strDestinationLevelName = 0;
            objBLevel.strSourceLevelName = @"Ground Level";
            [self.arrBuildingLevel addObject:objBLevel];
            NSString * tenant_level = [dics valueForKeyPath:@"tenant_level"];

            if (![self stringIsEmpty:tenant_level shouldCleanWhiteSpace:YES]) {
                NSArray * commands = nil;
                if( [tenant_level rangeOfString:@","].location != NSNotFound ){

                    commands = [tenant_level componentsSeparatedByString:@","];
                    int i = 2;
                        for(NSString * strLevelName in commands){
                            BuildingLevel * objBLevel = [[BuildingLevel alloc]init];
                            objBLevel.idx = i;
                            objBLevel.sourceLevel = [strLevelName intValue];
                            objBLevel.strDestinationLevelName = strLevelName;
                            objBLevel.strSourceLevelName = [NSString stringWithFormat:@"Level %@",strLevelName];
                            //NSLog(@"str name %@ and count %d",strCateName,count);
                            [self.arrBuildingLevel addObject:objBLevel];
                            i ++;
                        }
                }
                else {
                    BuildingLevel * objBLevel = [[BuildingLevel alloc]init];
                    objBLevel.idx = 2;
                    objBLevel.sourceLevel = [tenant_level intValue];
                    objBLevel.strDestinationLevelName = tenant_level;
                    objBLevel.strSourceLevelName = [NSString stringWithFormat:@"Level %@",tenant_level];
                    NSLog(@"str name %@ and count %d",tenant_level,objBLevel.idx);
                    [self.arrBuildingLevel addObject:objBLevel];
                }

            }

            [SVProgressHUD showSuccessWithStatus:@"Done"];
            }*/
    }
    else if (status == 2 ){
        //message = @"You are not reached the building!";
        //message = @"You are not connected building wifi!";
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info"
                                                       message:@"You must be within premises to use the application."
                                                      delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [delegate.db updateDeselectedBuilding];
        [delegate disableTheTabs:self.tabBarController];
        self.tabBarController.selectedIndex = 2;
        [SVProgressHUD dismiss];

    }
    else if (status == 3 ){
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info"
                                                       message:@"Please connect to the building's Wifi before you can proceed."
                                                      delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [delegate.db updateDeselectedBuilding];
        [delegate disableTheTabs:self.tabBarController];
        self.tabBarController.selectedIndex = 2;
        [SVProgressHUD dismiss];
    }
}

- (void) onErrorLoad: (int) processId{
    NSLog(@"error process %d",processId);
    [SVProgressHUD dismiss];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int)processId{

    if (processId == 2) {
        int status = [[dics valueForKeyPath:@"status"] intValue];
         NSString * message = [dics valueForKeyPath:@"message"];
        if (status == 1) {
            [self.arrBuildingLevel removeAllObjects];
            BuildingLevel * objBLevel = [[BuildingLevel alloc]init];
            objBLevel.idx = 1;
            objBLevel.sourceLevel = 0;
            objBLevel.strDestinationLevelName = 0;
            objBLevel.strSourceLevelName = @"Ground Level";
            [self.arrBuildingLevel addObject:objBLevel];
            NSString * tenant_level = [dics valueForKeyPath:@"tenant_level"];
            //NSArray * commands = nil;
            if (![self stringIsEmpty:tenant_level shouldCleanWhiteSpace:YES]) {
                NSArray * commands = nil;
                if( [tenant_level rangeOfString:@","].location != NSNotFound ){

                    commands = [tenant_level componentsSeparatedByString:@","];
                    int i = 2;
                    for(NSString * strLevelName in commands){
                        BuildingLevel * objBLevel = [[BuildingLevel alloc]init];
                        objBLevel.idx = i;
                        objBLevel.sourceLevel = [strLevelName intValue];
                        objBLevel.strDestinationLevelName = strLevelName;
                        objBLevel.strSourceLevelName = [NSString stringWithFormat:@"Level %@",strLevelName];
                        //NSLog(@"str name %@ and count %d",strCateName,count);
                        [self.arrBuildingLevel addObject:objBLevel];
                        i ++;
                    }
                }
                else {
                    BuildingLevel * objBLevel = [[BuildingLevel alloc]init];
                    objBLevel.idx = 2;
                    objBLevel.sourceLevel = [tenant_level intValue];
                    objBLevel.strDestinationLevelName = tenant_level;
                    objBLevel.strSourceLevelName = [NSString stringWithFormat:@"Level %@",tenant_level];
                    NSLog(@"str name %@ and count %d",tenant_level,objBLevel.idx);
                    [self.arrBuildingLevel addObject:objBLevel];
                }

            }
            [SVProgressHUD showSuccessWithStatus:@"Done"];
        }
        else if(status == 2 || status == 4) {
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Sync Error"
                                                           message:message
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
        }
        else if (status == 0){
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Sync Error"
                                                           message:@"Invalid based url or sever is down!"
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
        }

    }
    else if (processId == 3) {
        int status = [[dics valueForKeyPath:@"status"] intValue];
        NSString * message = [dics valueForKeyPath:@"message"];
        if (status == 1) {
            NSString * strLift = [dics valueForKeyPath:@"request_lift"];
            //self.lblLift.text = strLift;
            NSLog(@"str lift %@",strLift);
            self.objResultLevel.strLift = strLift;
            [SVProgressHUD dismiss];
            
            
            [self showProceedView:self.objResultLevel];
            //[SVProgressHUD showSuccessWithStatus:@"Done"];
        }
        else if(status == 2 || status == 4) {
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Sync Error"
                                                           message:message
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
        }
        else if (status == 0){
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Sync Error"
                                                           message:@"Invalid based url or sever is down!"
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
        }

        [SVProgressHUD dismiss];
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    if (pickerView.tag == 0) {

        BuildingLevel * objLevel = [self.arrBuildingLevel objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);
            return objLevel.strSourceLevelName;

    }
    else if (pickerView.tag == 1) {

        BuildingLevel * objLevel = [self.arrDestinationLevel objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);
        return objLevel.strSourceLevelName;

    }
    else if (pickerView.tag == 2) {

        NSString * str = [arrGroupCall objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);
        return str;

    }
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 0) {
        return [self.arrBuildingLevel count];
    }
    else if(pickerView.tag == 1){
        return [self.arrDestinationLevel count];
    }
    else if(pickerView.tag == 2){
        return [arrGroupCall count];
    }
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    NSLog(@"Chose %d", self.selectedDestinationLevel);
    if (pickerView.tag == 0) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedLevel = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
    }
    else if (pickerView.tag == 1) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedDestinationLevel = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
    }
    else if (pickerView.tag == 2) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedGroup = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
    }
    /*else if(pickerView.tag == 1){
     intFromIndex = row;
     }
     else if(pickerView.tag == 2){
     intToIndex = row;
     }
     else if(pickerView.tag == 3){
     intTimeIndex = row;
     }*/
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];

    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
        if (!self.isSelected) {
            [self enableTheDestination:NO];
        }
    }
    else if (buttonIndex == 1) {
        NSLog(@"Other Button Done Clicked and selected index %d",self.selectedLevel);

        if (self.uiPickerView.tag == 0){
            //Date picker click
            BuildingLevel * objLevel = [self.arrBuildingLevel objectAtIndex:self.selectedLevel];
            self.objSelectedLevel = objLevel;
            NSString * strSourceName;
            if (objLevel.sourceLevel == 0) {
                strSourceName = @"G";
            }
            else{
                strSourceName = [NSString stringWithFormat:@"%d",objLevel.sourceLevel];
            }

            [self.btnCurrent setTitle:strSourceName forState:normal];

            self.isSelected = TRUE;

            //[self showConfirmationDialog];
            [self enableTheDestination:YES];
//            if(self.arrDestinationLevel.count < self.arrBuildingLevel.count) {
//                self.arrDestinationLevel = [NSMutableArray arrayWithArray:self.arrBuildingLevel];
//            }
//            
//            BuildingLevel *checkerLevel = [self.arrBuildingLevel objectAtIndex:self.selectedLevel];
//            NSMutableArray *finalArray = [[NSMutableArray alloc] init];
//            int i = 0;
//            for(BuildingLevel *myBuildingLevel in self.arrDestinationLevel) {
//                if(![checkerLevel.strSourceLevelName isEqualToString:myBuildingLevel.strSourceLevelName]) {
//                    [finalArray addObject:myBuildingLevel];
//                }
//                i++;
//            }
            self.arrDestinationLevel = [NSMutableArray arrayWithArray:self.arrBuildingLevel];
//            NSLog(@"Final array %@", self.arrDestinationLevel);

            if ([self.arrDestinationLevel count] == 1) {
                BuildingLevel * objLevel = [self.arrDestinationLevel objectAtIndex:0];
                self.objDestinationLevel = objLevel;
                NSString * strSourceName;
                if (objLevel.sourceLevel == 0) {
                    strSourceName = @"G";
                }
                else{
                    strSourceName = [NSString stringWithFormat:@"%d",objLevel.sourceLevel];
                }

                [self.btnDestination setTitle:strSourceName forState:normal];
                self.isDestinationSelected = TRUE;
            } else {

            }
            
        }
        else if (self.uiPickerView.tag == 1){
            NSLog(@"%d", self.objDestinationLevel.sourceLevel);
            //Date picker click
            BuildingLevel * objLevel = [self.arrDestinationLevel objectAtIndex:self.selectedDestinationLevel];
            self.objDestinationLevel = objLevel;



            NSString * strSourceName;
            if (objLevel.sourceLevel == 0) {
                strSourceName = @"G";
            }
            else{
                strSourceName = [NSString stringWithFormat:@"%d",objLevel.sourceLevel];
            }

            [self.btnDestination setTitle:strSourceName forState:normal];

            self.isDestinationSelected = TRUE;
        }
        else if (self.uiPickerView.tag == 2){
            //Date picker click
            NSString * str = [arrGroupCall objectAtIndex:self.selectedGroup];
            [self.btnGroupCall setTitle:str forState:normal];
            self.isGroupSelected = TRUE;
        }

    }

}

- (IBAction)onLevel:(id)sender{
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    Building * building = [delegate.db getSelectedBuilding];
    #if IS_DEMO
        [self.menu setTitle:@"Select a level"];
        //UIButton * btn = (UIButton *)sender;
        // Add the picker
        self.uiPickerView.tag = 0;
        self.uiPickerView.delegate = self;
        [self.uiPickerView reloadAllComponents];
        [self.uiPickerView selectRow:self.selectedLevel inComponent:0 animated:YES];
        [self.menu addSubview:self.uiPickerView];
        [self.menu showInView:self.view];
        [self.menu setBounds:CGRectMake(0,0,320,600)];

    CGRect pickerRect = self.uiPickerView.bounds;
    pickerRect.origin.y = 35;
    self.uiPickerView.frame = pickerRect;
    #else
    if([delegate validateBuilding:building]) {
        if ([self.arrBuildingLevel count]>0) {
            [self.menu setTitle:@"Select a level"];
            //UIButton * btn = (UIButton *)sender;
            // Add the picker
            self.uiPickerView.tag = 0;
            self.uiPickerView.delegate = self;
            [self.uiPickerView reloadAllComponents];
            [self.uiPickerView selectRow:self.selectedLevel inComponent:0 animated:YES];
            [self.menu addSubview:self.uiPickerView];
            [self.menu showInView:self.view];
            [self.menu setBounds:CGRectMake(0,0,320,600)];
        } else {
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Lift"
                                                           message:@"Please add a building before you can proceed."
                                                          delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    } else {
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Lift"
                                                       message:@"You must be within premises to use the application."
                                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    #endif
}

-(IBAction)onDLevel:(id)sender{
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    Building * building = [delegate.db getSelectedBuilding];

#if IS_DEMO
    [self.menu setTitle:@"Select Destination level"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 1;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedDestinationLevel inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,600)];
#else
    if([delegate validateBuilding:building]) {
        if ([self.arrBuildingLevel count]>0) {
            [self.menu setTitle:@"Select Destination level"];
            //UIButton * btn = (UIButton *)sender;
            // Add the picker
            self.uiPickerView.tag = 1;
            self.uiPickerView.delegate = self;
            [self.uiPickerView reloadAllComponents];
            [self.uiPickerView selectRow:self.selectedDestinationLevel inComponent:0 animated:YES];
            [self.menu addSubview:self.uiPickerView];
            [self.menu showInView:self.view];
            [self.menu setBounds:CGRectMake(0,0,320,600)];
        } else {
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Lift"
                                                           message:@"Please add a building before you can proceed."
                                                          delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    } else {
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Lift"
                                                       message:@"You must be within premises to use the application."
                                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
#endif


}

-(IBAction)onGroup:(id)sender{
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    Building * building = [delegate.db getSelectedBuilding];
    if([delegate validateBuilding:building]) {
        [self.menu setTitle:@"Please Select Number of Passengers"];
        //UIButton * btn = (UIButton *)sender;
        // Add the picker
        self.uiPickerView.tag = 2;
        self.uiPickerView.delegate = self;
        [self.uiPickerView reloadAllComponents];
        [self.uiPickerView selectRow:self.selectedGroup inComponent:0 animated:YES];
        [self.menu addSubview:self.uiPickerView];
        [self.menu showInView:self.view];
        [self.menu setBounds:CGRectMake(0,0,320,600)];
    } else {
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Lift"
                                                       message:@"You must be within premises to use the application."
                                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

}

-(IBAction)onProceed:(id)sender {
    if (self.arrBuildingLevel.count == 0 || self.arrDestinationLevel.count == 0) {
        return;
    }
    LiftAppDelegate * delegate= [[UIApplication sharedApplication] delegate];
    Building * objBuilding = [delegate.db getSelectedBuilding];
    BuildingLevel * sLevel = [self.arrBuildingLevel objectAtIndex:self.selectedLevel];
    BuildingLevel * dLevel = [self.arrDestinationLevel objectAtIndex:self.selectedDestinationLevel];
    if (sLevel.sourceLevel == dLevel.sourceLevel) {
        [Utility showAlert:@"Hitachi" message:@"Please select a different floor."];
        return;
    }
#if IS_DEMO
    [SVProgressHUD show];
    //self.objDestinationLevel.destinationLevel = self.objDestinationLevel.sourceLevel;
    //self.objDestinationLevel.sourceLevel = self.objSelectedLevel.sourceLevel;
    
    
    self.objResultLevel.strHashTenantCode = objBuilding.strHashTenantCode;
    self.objResultLevel.strType = MOBILE_T_TYPE;
    
    self.objResultLevel.sourceLevel = sLevel.sourceLevel;
    self.objResultLevel.destinationLevel = dLevel.sourceLevel;
    
    self.objResultLevel.strSourceLevelName = sLevel.strSourceLevelName;
    self.objResultLevel.strDestinationLevelName = dLevel.strSourceLevelName;
    
    NSLog(@"source level %@ and d level %@",self.objResultLevel.strSourceLevelName,self.objResultLevel.strDestinationLevelName);
    
    [self performSelector:@selector(finishProccessForDemo) withObject:nil afterDelay:1];
#else
    NSInteger myInt = objBuilding.strHashTenantCode.length;
    //NSLog(@"error return %@ and string length count %d",objBuilding.strHashTenantCode,myInt);
    self.objResultLevel.strHashTenantCode = objBuilding.strHashTenantCode;
    self.objResultLevel.strType = MOBILE_T_TYPE;
    
    self.objResultLevel.sourceLevel = sLevel.sourceLevel;
    self.objResultLevel.destinationLevel = dLevel.sourceLevel;
    
    self.objResultLevel.strSourceLevelName = sLevel.strSourceLevelName;
    self.objResultLevel.strDestinationLevelName = dLevel.strSourceLevelName;
    
    _requestLift = [[SOAPRequest alloc] initWithOwner:self];
    
    _requestLift.processId = 3;
    [_requestLift syncLift:self.objResultLevel];
    [SVProgressHUD show];
#endif
}

- (void)finishProccessForDemo{
    int imgCode = (arc4random() % 2);
    NSString * str = @"";
    if (imgCode == 0) {
        str = @"Lift A";
    }
    else if(imgCode == 1){
        str = @"Lift B";
    }
    else if(imgCode == 2){
        str = @"Lift C";
    }
    self.objResultLevel.strLift = str;
    [SVProgressHUD dismiss];
    [self showProceedView:self.objResultLevel];
}

-(IBAction)onOk:(id)sender{
    if (self.isSelected) {
       /* NSLog(@"here it is!!");
        [self.arrBuildingLevel removeObjectAtIndex:self.selectedLevel];
        LFDestinationViewController * lfDesViewCotroller=[[LFDestinationViewController alloc] initWithNibName:@"LFDestinationViewController" bundle:[NSBundle mainBundle]];
        lfDesViewCotroller.arrDestinationLevel = self.arrBuildingLevel;
        lfDesViewCotroller.objSourceLevel = self.objSelectedLevel;
        [self.navigationController pushViewController:lfDesViewCotroller animated:YES];*/
        [self showConfirmationDialog];
    }
    else{
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Lift"
                                                       message:@"Please select a level before you can proceed."
                                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (void) showConfirmationDialog{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:@"Proceed?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.delegate = self;
    alert.tag = 0;
    [alert show];
}

- (IBAction)onSetting:(id)sender{
//    [self dismissViewControllerAnimated:YES completion:^{
//    }];
    [self performSegueWithIdentifier:@"toSetting" sender:nil];
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex{
    if( [alertView tag] == 0 ){
		if(buttonIndex == 0 ){

		}
		else if(buttonIndex == 1) {
            NSLog(@"here it is!!");
            [self.arrBuildingLevel removeObjectAtIndex:self.selectedLevel];
            LFDestinationViewController * lfDesViewCotroller=[[LFDestinationViewController alloc] initWithNibName:@"LFDestinationViewController" bundle:[NSBundle mainBundle]];
            NSLog(@"Arrays : %@", self.arrBuildingLevel);
            lfDesViewCotroller.arrDestinationLevel = self.arrBuildingLevel;
            lfDesViewCotroller.objSourceLevel = self.objSelectedLevel;
            [self.navigationController pushViewController:lfDesViewCotroller animated:YES];
		}
	}

}

- (BOOL ) stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL)cleanWhileSpace {

    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }

    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    }

    if (cleanWhileSpace) {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }

    return NO;
}

- (void) showProceedView:(BuildingLevel *)obj{
    if (fxBlurView == nil) {
        fxBlurView = [[FXBlurView alloc] initWithFrame:self.view.frame];
    }
    //fxBlurView.backgroundColor = [UIColor whiteColor];
    fxBlurView.blurRadius = 4;
    fxBlurView.tintColor = [UIColor darkGrayColor];

    [self.view addSubview:fxBlurView];
    [self.view bringSubviewToFront:proceedCardView];
    proceedCardView.hidden = FALSE;
    [proceedCardView loadContentSuccessWith:obj];
    CGRect originalFrame = proceedCardView.frame;
    originalFrame.origin.y = 154;
    //[UIView setAnimationCurve:UIViewAnimationTransitionCurlUp];
    [UIView animateWithDuration:0.5 animations:^{
        [proceedCardView setFrame:originalFrame];
    } completion:^(BOOL finished) {
    }];

    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         // ... do stuff here
                         [proceedCardView setFrame:originalFrame];
                     } completion:NULL];

    // setup secondView to be partialy rotated and invisible
//    proceedCardView.layer.transform = [self makeRotationAndPerspectiveTransform:M_PI/2];
//    proceedCardView.hidden = YES;
//    CFTimeInterval duration =  2.0;
//
//    [UIView animateWithDuration:duration /2
//                          delay:0
//                        options:UIViewAnimationCurveEaseOut
//                     animations:^{
//                         proceedCardView.hidden = NO;
//                         proceedCardView.frame = originalFrame;
//                         proceedCardView.layer.transform =  CATransform3DMakeRotation(0.0f, 1.0f, 0.0f, 0.0f);
//                     }
//                     completion:NULL];

}

- (CATransform3D) makeRotationAndPerspectiveTransform:(CGFloat) angle {
    CATransform3D transform = CATransform3DMakeRotation(angle, 1.0f, 0.0f, 0.0f);
    transform.m34 = 1.0 / -500;
    return transform;
}

- (void) onProceedClose:(ProceedCardView *)proceedView{
    CGRect originalFrame = proceedCardView.frame;
    originalFrame.origin.y = 568;
    [UIView animateWithDuration:0.3 animations:^{
        [proceedCardView setFrame:originalFrame];
    } completion:^(BOOL finished) {
        proceedCardView.hidden = TRUE;
        if (fxBlurView != nil) {
            [fxBlurView removeFromSuperview];
        }
    }];
    [self reverseLoad];
}

- (void) viewDidDisappear:(BOOL)animated{
    NSLog(@"Remove");
    [self.arrBuildingLevel removeAllObjects];
    [self.arrDestinationLevel removeAllObjects];
}

- (void) reverseLoad{
    if (self.arrBuildingLevel.count == 0 || self.arrDestinationLevel.count == 0) {
        return;
    }
    NSLog(@"Reverse");
    NSLog(@"selected level %d and des level %d",self.selectedLevel,self.selectedDestinationLevel);
    int tempSelectedLevel = self.selectedLevel;
    self.selectedLevel = self.selectedDestinationLevel;
    self.selectedDestinationLevel = tempSelectedLevel;
    
    NSMutableArray *tempArr = self.arrBuildingLevel;
    self.arrBuildingLevel = self.arrDestinationLevel;
    self.arrDestinationLevel = tempArr;
    
    //Date picker click
    NSLog(@"%@ - %d", self.arrBuildingLevel, self.selectedLevel);
    BuildingLevel * objLevel = [self.arrBuildingLevel objectAtIndex:self.selectedLevel];
    self.objSelectedLevel = objLevel;
    NSString * strSourceName;
    if (objLevel.sourceLevel == 0) {
        strSourceName = @"G";
    }
    else{
        strSourceName = [NSString stringWithFormat:@"%d",objLevel.sourceLevel];
    }
    [self.btnCurrent setTitle:strSourceName forState:normal];
    
    self.isSelected = TRUE;
    [self enableTheDestination:YES];
    
    
    //Destination Date picker click
    BuildingLevel * objLevel2 = [self.arrDestinationLevel objectAtIndex:self.selectedDestinationLevel];
    self.objDestinationLevel = objLevel2;
    
    NSString * strSourceName2;
    if (objLevel2.sourceLevel == 0) {
        strSourceName2 = @"G";
    }
    else{
        strSourceName2 = [NSString stringWithFormat:@"%d",objLevel2.sourceLevel];
    }
    
    [self.btnDestination setTitle:strSourceName2 forState:normal];
    self.isDestinationSelected = TRUE;
}

- (IBAction)onReverse:(id)sender{
    [self reverseLoad];
}

- (void)onActionDone:(id)sender{
    [self actionSheet:self.menu clickedButtonAtIndex:1];
    [self.menu dismissWithClickedButtonIndex:1 animated:YES];

}

- (void)onActionCancel:(id)sender{
    [self actionSheet:self.menu clickedButtonAtIndex:0];
    [self.menu dismissWithClickedButtonIndex:0 animated:YES];
}

@end

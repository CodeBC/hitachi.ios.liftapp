//
//  LiftProceedViewController.h
//  Lift
//
//  Created by Zayar on 4/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BuildingLevel.h"
@interface LiftProceedViewController : UIViewController
@property (nonatomic, strong) BuildingLevel * objBuildingLevel;
@end

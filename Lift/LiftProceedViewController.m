//
//  LiftProceedViewController.m
//  Lift
//
//  Created by Zayar on 4/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "LiftProceedViewController.h"
#import "BuildingLevel.h"
#import "Building.h"
#import "LiftAppDelegate.h"
#import "SOAPRequest.h"

@interface LiftProceedViewController ()
@property (nonatomic, strong) IBOutlet UILabel * lblSLevel;
@property (nonatomic, strong) IBOutlet UILabel * lblDLevel;
@property (nonatomic, strong) IBOutlet UILabel * lblLift;
@property (nonatomic, strong) SOAPRequest * requestLift;
@end

@implementation LiftProceedViewController
@synthesize objBuildingLevel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setTitle:@"Successful Registration"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //self.navigationItem.title = @"Successfull Regsiter";
}

- (void)viewWillAppear:(BOOL)animated{
    if (objBuildingLevel != nil) {
        self.lblSLevel.text = objBuildingLevel.strSourceLevelName;
        self.lblDLevel.text = objBuildingLevel.strDestinationLevelName;
        
        NSLog(@"Destination Level %d", objBuildingLevel.destinationLevel);
        
        self.lblLift.text = @"";
        LiftAppDelegate * delegate= [[UIApplication sharedApplication] delegate];
        Building * objBuilding = [delegate.db getSelectedBuilding];
        objBuildingLevel.strHashTenantCode = objBuilding.strHashTenantCode;
        NSInteger myInt = objBuildingLevel.strHashTenantCode.length;
        NSLog(@"error return %@ and string length count %d",objBuildingLevel.strHashTenantCode,myInt);
        
        _requestLift = [[SOAPRequest alloc] initWithOwner:self];
        
        _requestLift.processId = 3;
        [_requestLift syncLift:objBuildingLevel];
        [SVProgressHUD show];
    }
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int)processId{
    if (processId == 3) {
        int status = [[dics valueForKeyPath:@"status"] intValue];
        NSString * message = [dics valueForKeyPath:@"message"];
        if (status == 1) {
            NSString * strLift = [dics valueForKeyPath:@"request_lift"];
            self.lblLift.text = strLift;
            [SVProgressHUD showSuccessWithStatus:@"Done"];
        }
        else if(status == 2 || status == 4) {
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Sync Error"
                                                           message:message
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
        }
        else if (status == 0){
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Sync Error"
                                                           message:@"Invalid based url or sever is down!"
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
        }
        
        [SVProgressHUD dismiss];
    }
}

- (void) onErrorLoad: (int) processId{
    NSLog(@"error process %d",processId);
    [SVProgressHUD dismiss];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

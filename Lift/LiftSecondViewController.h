//
//  LiftSecondViewController.h
//  Lift
//
//  Created by Zayar on 4/2/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
@interface LiftSecondViewController : UIViewController<SOAPRequestDelegate>
-(IBAction)onLevel:(id)sender;
@end

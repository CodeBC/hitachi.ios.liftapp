//
//  LiftSecondViewController.m
//  Lift
//
//  Created by Zayar on 4/2/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "LiftSecondViewController.h"
#import "Building.h"
#import "LiftAppDelegate.h"
#import "SOAPRequest.h"
#import "BuildingLevel.h"
#import "StringTable.h"
#import "LiftProceedViewController.h"
#import "JSONKit.h"
@interface LiftSecondViewController ()
@property (nonatomic, strong) UIPickerView *uiPickerView;
@property (nonatomic, strong) UIActionSheet *menu;
@property int selectedLevel;
@property (nonatomic, strong) IBOutlet UIButton * btnDropDown;
@property (nonatomic, strong) NSMutableArray * arrBuildingLevel;
@property (nonatomic, strong) SOAPRequest * requestBuildingAccess;
@property BOOL isSelected;
@property (nonatomic, strong) BuildingLevel * objSelectedLevel;
@property BOOL isBack;
@end

@implementation LiftSecondViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,170,0,0)];
    
    self.uiPickerView.delegate = self;
    self.uiPickerView.showsSelectionIndicator = YES;// note this is default to NO
    
    
    self.menu = [[UIActionSheet alloc] initWithTitle:@"Building"
                                            delegate:self
                                   cancelButtonTitle:@"Done"
                              destructiveButtonTitle:@"Cancel"
                                   otherButtonTitles:nil];
    self.selectedLevel = 0;
    self.arrBuildingLevel = [[NSMutableArray alloc]init];
    self.isBack = FALSE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated{
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate disableTheTabs:self.tabBarController];
    
}

- (void)viewDidAppear:(BOOL)animated{
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    Building * building = [delegate.db getSelectedBuilding];
    if (building != nil) {
        int status = [delegate validateBuilding:building];
        
        if (status == 1) {
             _requestBuildingAccess = [[SOAPRequest alloc] initWithOwner:self];
             _requestBuildingAccess.processId = 2;
             [_requestBuildingAccess syncAccessBuilding:building.strHashTenantCode];
             [SVProgressHUD show];
           /* NSString * strPath = [[NSBundle mainBundle]pathForResource:@"tanent_level" ofType:@"json"];
            NSData * jsonData= [NSData dataWithContentsOfFile:strPath];
            NSDictionary *dics;
            JSONDecoder* decoder = [[JSONDecoder alloc]
                                    initWithParseOptions:JKParseOptionNone];
            //NSMutableArray * arrTemp = [[NSMutableArray alloc]init];
            dics = [decoder objectWithData:jsonData];
            int status = [[dics valueForKeyPath:@"status"] intValue];
            //NSString * message = [dics valueForKeyPath:@"message"];
            if (status == 1) {
                [self.arrBuildingLevel removeAllObjects];
                NSString * tenant_level = [dics valueForKeyPath:@"tenant_level"];
                
                if (![self stringIsEmpty:tenant_level shouldCleanWhiteSpace:YES]) {
                    NSArray * commands = nil;
                    if( [tenant_level rangeOfString:@","].location != NSNotFound ){
                        
                        commands = [tenant_level componentsSeparatedByString:@","];
                        int i = 2;
                        for(NSString * strLevelName in commands){
                            BuildingLevel * objBLevel = [[BuildingLevel alloc]init];
                            objBLevel.idx = i;
                            objBLevel.sourceLevel = [strLevelName intValue];
                            objBLevel.strDestinationLevelName = strLevelName;
                            objBLevel.strSourceLevelName = [NSString stringWithFormat:@"Level %@",strLevelName];
                            //NSLog(@"str name %@ and count %d",strCateName,count);
                            [self.arrBuildingLevel addObject:objBLevel];
                            i ++;
                        }
                    }
                    else {
                        BuildingLevel * objBLevel = [[BuildingLevel alloc]init];
                        objBLevel.idx = 2;
                        objBLevel.sourceLevel = [tenant_level intValue];
                        objBLevel.strDestinationLevelName = tenant_level;
                        objBLevel.strSourceLevelName = [NSString stringWithFormat:@"Level %@",tenant_level];
                        NSLog(@"str name %@ and count %d",tenant_level,objBLevel.idx);
                        [self.arrBuildingLevel addObject:objBLevel];
                    }
                    
                    if (!self.isBack) {
                        if ([self.arrBuildingLevel count] == 1) {
                            self.objSelectedLevel = 0;
                            [self goToNext];
                        }
                    }
                }
            }*/
        }
        else if (status == 2 ){
            //message = @"You are not reached the building!";
            //message = @"You are not connected building wifi!";
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info"
                                                           message:@"You must be within premises to use the application."
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
            [delegate.db updateDeselectedBuilding];
            [delegate disableTheTabs:self.tabBarController];
            self.tabBarController.selectedIndex = 2;
        }
        else if (status == 3){
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info"
                                                           message:@"Please connect to the building's Wifi before you can proceed."
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
            [delegate.db updateDeselectedBuilding];
            [delegate disableTheTabs:self.tabBarController];
            self.tabBarController.selectedIndex = 2;
        }
    }
    else{
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info"
                                                       message:@"Please select or add a building before you can proceed."
                                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        self.tabBarController.selectedIndex = 2;
    }
    self.isSelected = FALSE;
}

- (void) showConfirmationDialog{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:@"Proceed?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.delegate = self;
    alert.tag = 0;
    [alert show];
}

- (void) onErrorLoad: (int) processId{
    NSLog(@"error process %d",processId);
    [SVProgressHUD dismiss];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int)processId{
    if (processId == 2) {
        int status = [[dics valueForKeyPath:@"status"] intValue];
        NSString * message = [dics valueForKeyPath:@"message"];
        if (status == 1) {
            [self.arrBuildingLevel removeAllObjects];
            NSString * tenant_level = [dics valueForKeyPath:@"tenant_level"];

            if (![self stringIsEmpty:tenant_level shouldCleanWhiteSpace:YES]) {
                NSArray * commands = nil;
                if( [tenant_level rangeOfString:@","].location != NSNotFound ){
                    
                    commands = [tenant_level componentsSeparatedByString:@","];
                    int i = 2;
                    for(NSString * strLevelName in commands){
                        BuildingLevel * objBLevel = [[BuildingLevel alloc]init];
                        objBLevel.idx = i;
                        objBLevel.sourceLevel = [strLevelName intValue];
                        objBLevel.strDestinationLevelName = strLevelName;
                        objBLevel.strSourceLevelName = [NSString stringWithFormat:@"Level %@",strLevelName];
                        //NSLog(@"str name %@ and count %d",strCateName,count);
                        [self.arrBuildingLevel addObject:objBLevel];
                        i ++;
                    }
                }
            }
            if([tenant_level length] == 1) {
                BuildingLevel * objBLevel = [[BuildingLevel alloc]init];
                objBLevel.idx = 2;
                objBLevel.sourceLevel = [tenant_level intValue];
                objBLevel.strDestinationLevelName = tenant_level;
                objBLevel.strSourceLevelName = [NSString stringWithFormat:@"Level %@",tenant_level];
                NSLog(@"str name %@ and count %d",tenant_level,objBLevel.idx);
                [self.arrBuildingLevel addObject:objBLevel];
            }
            
            if (!self.isBack) {
                if ([self.arrBuildingLevel count] == 1) {
                    BuildingLevel * objLevel = [self.arrBuildingLevel objectAtIndex:0];
                    self.objSelectedLevel = objLevel;
                    [self goToNext];
                }
            }

            [SVProgressHUD showSuccessWithStatus:@"Done"];
        }
        else if(status == 2 || status == 4) {
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Sync Error"
                                                           message:message
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
        }
        else if (status == 0){
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Sync Error"
                                                           message:@"Invalid based url or sever is down!"
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
        }
        
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (pickerView.tag == 0) {
        
        BuildingLevel * objLevel = [self.arrBuildingLevel objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);
        return objLevel.strSourceLevelName;
        
    }
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return [self.arrBuildingLevel count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    if (pickerView.tag == 0) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedLevel = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
    }
    
    /*else if(pickerView.tag == 1){
     intFromIndex = row;
     }
     else if(pickerView.tag == 2){
     intToIndex = row;
     }
     else if(pickerView.tag == 3){
     intTimeIndex = row;
     }*/
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
    }
    
    else if (buttonIndex == 1) {
        NSLog(@"Other Button Done Clicked and selected index %d",self.selectedLevel);
        
        if (self.uiPickerView.tag == 0){
            //Date picker click
            BuildingLevel * objLevel = [self.arrBuildingLevel objectAtIndex:self.selectedLevel];
            self.objSelectedLevel = objLevel;
            [self.btnDropDown setTitle:objLevel.strSourceLevelName forState:normal];
            
            self.isSelected = TRUE;
            
            [self showConfirmationDialog];
        }
    }
}

-(IBAction)onLevel:(id)sender{
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    Building * building = [delegate.db getSelectedBuilding];
    if([delegate validateBuilding:building]) {
        
        [self.menu setTitle:@"Select a level"];
        //UIButton * btn = (UIButton *)sender;
        // Add the picker
        self.uiPickerView.tag = 0;
        self.uiPickerView.delegate = self;
        [self.uiPickerView reloadAllComponents];
        [self.uiPickerView selectRow:self.selectedLevel inComponent:0 animated:YES];
        [self.menu addSubview:self.uiPickerView];
        [self.menu showInView:self.view];
        [self.menu setBounds:CGRectMake(0,0,320,600)];
        
    } else {
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Lift"
                                                       message:@"You must be within premises to use the application."
                                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(IBAction)onOk:(id)sender{
    if (self.isSelected) {
        /*NSLog(@"here it is!!");
        
        self.objSelectedLevel.strDestinationLevelName = @"Ground Level";
        self.objSelectedLevel.sourceLevel =0;
        
        self.objSelectedLevel.strType = VISSITOR_TYPE;
        
        LiftProceedViewController * lfProceedViewCotroller=[[LiftProceedViewController alloc] initWithNibName:@"LiftProceedViewController" bundle:[NSBundle mainBundle]];
        lfProceedViewCotroller.objBuildingLevel = self.objSelectedLevel;
        [self.navigationController pushViewController:lfProceedViewCotroller animated:YES];*/
        [self showConfirmationDialog];
    }
    else{
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Lift"
                                                       message:@"You did not select a Level. Please select a level!"
                                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex{
    if( [alertView tag] == 0 ){
		if(buttonIndex == 0 ){
            
		}
		else if(buttonIndex == 1) {
            NSLog(@"here it is!!");
             
            [self goToNext];
		}
	}

}

- (void)goToNext{
    self.objSelectedLevel.strDestinationLevelName = @"Ground Level";
    self.objSelectedLevel.sourceLevel =0;
    
    self.objSelectedLevel.strType = VISSITOR_TYPE;
    
    LiftProceedViewController * lfProceedViewCotroller=[[LiftProceedViewController alloc] initWithNibName:@"LiftProceedViewController" bundle:[NSBundle mainBundle]];
    lfProceedViewCotroller.objBuildingLevel = self.objSelectedLevel;
    [self.navigationController pushViewController:lfProceedViewCotroller animated:YES];
    self.isBack = TRUE;
}

- (BOOL ) stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL)cleanWhileSpace {
    
    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }
    
    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    }
    
    if (cleanWhileSpace) {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }
    
    return NO;
}

@end

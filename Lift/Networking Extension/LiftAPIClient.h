//
//  LiftAPIClient.h
//  Lift
//
//  Created by Zayar on 4/4/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "AFHTTPClient.h"

@interface LiftAPIClient : AFHTTPClient
+ (LiftAPIClient *)sharedClient;
@end

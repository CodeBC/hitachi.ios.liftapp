//
//  LiftAPIClient.m
//  Lift
//
//  Created by Zayar on 4/4/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "LiftAPIClient.h"
#import "AFJSONRequestOperation.h"
static NSString * const kAFAppDotNetAPIBaseURLString = @"http://www.iwh.com.sg/sl";
@implementation LiftAPIClient
+ (LiftAPIClient *)sharedClient {
    static LiftAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[LiftAPIClient alloc] initWithBaseURL:[NSURL URLWithString:kAFAppDotNetAPIBaseURLString]];
    });
    
    return _sharedClient;
}

- (id)initWithBaseURL:(NSURL *)url {
    
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    
    // Accept HTTP Header; see http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.1
	[self setDefaultHeader:@"Accept" value:@"application/json"];
    
    return self;
}

@end

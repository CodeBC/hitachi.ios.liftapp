//
//  ObjectConfig.h
//  KBZ
//
//  Created by Zayar Cn on 11/2/11.
//  Copyright (c) 2011 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjectConfig : NSObject
{
    int idx;
    NSString * strBaseLink;

}
@property int idx;
@property (nonatomic,strong) NSString * strBaseLink;
@end

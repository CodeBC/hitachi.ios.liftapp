//
//  ProceedCardView.h
//  Lift
//
//  Created by Zayar on 11/22/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BuildingLevel.h"
@protocol ProceedCardViewDelegate;
@interface ProceedCardView : UIView
{
    id<ProceedCardViewDelegate> owner;
}
@property id<ProceedCardViewDelegate> owner;
- (void) setUpViewsForSuccess;
- (void) loadContentSuccessWith:(BuildingLevel *)obj;
@end

@protocol ProceedCardViewDelegate
- (void) onProceedClose:(ProceedCardView *)proceedView;
@end

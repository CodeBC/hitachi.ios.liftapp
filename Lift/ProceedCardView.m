//
//  ProceedCardView.m
//  Lift
//
//  Created by Zayar on 11/22/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "ProceedCardView.h"
#import "Utility.h"
#import "BuildingLevel.h"
@interface ProceedCardView ()
{
    UILabel * lblTitle;

    UILabel * lblSourceLevel;
    UILabel * lblDestinationLevel;
    UILabel * bgSourceView;
    UIImageView * imgDestinationView;
    
    UILabel * capLblPlsPrTo;
    UILabel * lblLift;
    UIButton * btnClose;
    UIView * liftBgView;
    UILabel * lblCurrentLocation;
}
@end
@implementation ProceedCardView
@synthesize owner;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) setUpViewsForSuccess{
    self.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
    if (lblTitle == nil) {
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.frame.size.width, 30)];
    }
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.textColor = [UIColor colorWithHexString:@"343434"];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = [UIFont systemFontOfSize:20];
    lblTitle.text = @"Your call is accepted";
    [self addSubview:lblTitle];
    
    if (capLblPlsPrTo == nil) {
        capLblPlsPrTo = [[UILabel alloc] initWithFrame:CGRectMake(0, lblTitle.frame.origin.y + lblTitle.frame.size.height + 15,self.frame.size.width, 30)];
    }
    capLblPlsPrTo.textAlignment = NSTextAlignmentCenter;
    capLblPlsPrTo.textColor = [UIColor colorWithHexString:@"343434"];
    capLblPlsPrTo.backgroundColor = [UIColor clearColor];
    capLblPlsPrTo.font = [UIFont systemFontOfSize:13];
    capLblPlsPrTo.text = @"Please Proceed To";
    [self addSubview:capLblPlsPrTo];
    
    if (!liftBgView) {
        liftBgView = [[UIView alloc] initWithFrame:CGRectMake(58, capLblPlsPrTo.frame.origin.y + capLblPlsPrTo.frame.size.height + 10,166, 100)];
    }
    liftBgView.backgroundColor = [UIColor colorWithHexString:@"6ebe9f"];
    [Utility makeCornerRadius:liftBgView andRadius:5];
    [self addSubview:liftBgView];
    
    if (lblLift == nil) {
        lblLift = [[UILabel alloc] initWithFrame:CGRectMake(58, capLblPlsPrTo.frame.origin.y + capLblPlsPrTo.frame.size.height + 10,166, 45)];
    }
    lblLift.textAlignment = NSTextAlignmentCenter;
    lblLift.textColor = [UIColor colorWithHexString:@"ffffff"];
    lblLift.backgroundColor = [UIColor colorWithHexString:@"6ebe9f"];
    lblLift.font = [UIFont systemFontOfSize:30];
    [Utility makeCornerRadius:lblLift andRadius:5];
    [self addSubview:lblLift];
    
    if (!lblCurrentLocation) {
        lblCurrentLocation = [[UILabel alloc] initWithFrame:CGRectMake(58, lblLift.frame.origin.y + lblLift.frame.size.height,166, 45)];
    }
    lblCurrentLocation.textAlignment = NSTextAlignmentCenter;
    lblCurrentLocation.textColor = [UIColor colorWithHexString:@"ffffff"];
    lblCurrentLocation.backgroundColor = [UIColor colorWithHexString:@"6ebe9f"];
    lblCurrentLocation.font = [UIFont systemFontOfSize:15];
    lblCurrentLocation.text = @"Current location:";
    lblCurrentLocation.numberOfLines = 0;
    [Utility makeCornerRadius:lblCurrentLocation andRadius:5];
    [self addSubview:lblCurrentLocation];
    
    btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnClose setFrame:CGRectMake(238, 0, 50, 50)];
    [btnClose setImage:[UIImage imageNamed:@"btn_proceed_close"] forState:normal];
    [btnClose addTarget:self action:@selector(onClose:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnClose];
    
    if (bgSourceView == nil) {
        bgSourceView = [[UILabel alloc] initWithFrame:CGRectMake(0, liftBgView.frame.origin.y + liftBgView.frame.size.height + 15, self.frame.size.width, 40)];
    }
    bgSourceView.text = @"";
    bgSourceView.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
    bgSourceView.font = [UIFont systemFontOfSize:15];
    bgSourceView.textAlignment = NSTextAlignmentCenter;
    bgSourceView.numberOfLines = 0;
    [self addSubview:bgSourceView];
    
    /*if (imgDestinationView == nil) {
        imgDestinationView = [[UIImageView alloc] initWithFrame:CGRectMake(bgSourceView.frame.size.width - 148, liftBgView.frame.origin.y + liftBgView.frame.size.height + 15,bgSourceView.frame.size.width - 128 , 44)];
    }
    [imgDestinationView setImage:[UIImage imageNamed:@"bg_desitnation"]];
    [self addSubview:imgDestinationView];
    
    if (lblSourceLevel == nil) {
        lblSourceLevel = [[UILabel alloc] initWithFrame:CGRectMake(bgSourceView.frame.origin.x, bgSourceView.frame.origin.y + 5,bgSourceView.frame.size.width - 128 , 30)];
    }
    lblSourceLevel.textAlignment = NSTextAlignmentCenter;
    lblSourceLevel.textColor = [UIColor colorWithHexString:@"ffffff"];
    lblSourceLevel.backgroundColor = [UIColor clearColor];
    lblSourceLevel.font = [UIFont systemFontOfSize:15];
    [self addSubview:lblSourceLevel];
    
    if (lblDestinationLevel == nil) {
        lblDestinationLevel = [[UILabel alloc] initWithFrame:CGRectMake(imgDestinationView.frame.origin.x + 22, imgDestinationView.frame.origin.y + 5,imgDestinationView.frame.size.width - 22 , 30)];
    }
    lblDestinationLevel.textAlignment = NSTextAlignmentCenter;
    lblDestinationLevel.textColor = [UIColor colorWithHexString:@"ffffff"];
    lblDestinationLevel.backgroundColor = [UIColor clearColor];
    lblDestinationLevel.font = [UIFont systemFontOfSize:15];
    [self addSubview:lblDestinationLevel];*/
}

- (void) loadContentSuccessWith:(BuildingLevel *)obj{
    //lblSourceLevel.text = obj.strSourceLevelName;
    //lblDestinationLevel.text = obj.strDestinationLevelName;
    lblLift.text = obj.strLift;
    [self setCurrentLocationText:obj.strSourceLevelName];
    [self setFromToText:obj.strSourceLevelName andTo:obj.strDestinationLevelName];
}

- (void) onClose:(id)sender{
    [owner onProceedClose:self];
}

- (void) setCurrentLocationText:(NSString *)str{
    lblCurrentLocation.text = [NSString stringWithFormat:@"Current location: \n%@",str];
}

- (void) setFromToText:(NSString *)strFrom andTo:(NSString *)strTo{
    bgSourceView.text = [NSString stringWithFormat:@"Going from %@ to %@",strFrom,strTo];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

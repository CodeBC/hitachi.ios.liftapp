//
//  SOAPRequest.h
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BuildingLevel.h"  
@protocol SOAPRequestDelegate
- (void) onDataLoad: (int) processId withStatus:(int) status;
- (void) onErrorLoad: (int) processId;
- (void) onJsonLoaded:(NSMutableDictionary *) dics;
- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId;
- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId andResponsestatus:(int)status;
@end

typedef enum {
	REQUEST_TYPE_BUIDLING_REG = 1,
    REQUEST_TYPE_BUIDLING_ACCESS = 2,
    REQUEST_TYPE_LIFT = 3
    
} RequestType;

@interface SOAPRequest : NSObject<SOAPRequestDelegate,NSURLConnectionDelegate,NSURLConnectionDataDelegate> {
    NSMutableData * responseData;
    
    int processId;
    id <SOAPRequestDelegate> owner;
    
    RequestType rType;
    int intResponseStatusCode;
}

@property RequestType rType;
@property (nonatomic, retain) NSMutableData * responseData;
@property id<SOAPRequestDelegate> owner;
@property int processId;

- (id)initWithOwner:(id)del;
- (NSString *) urlencode: (NSString *) url;
- (void) syncAddBuilding:(NSString *)tMobileNo;
- (void) syncAccessBuilding:(NSString *)tHashCode;
- (void) syncLift:(BuildingLevel *)objBLevel;
@end

//
//  SOAPRequest.m
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//
#import <CommonCrypto/CommonDigest.h>
#import "SOAPRequest.h"
#import "JSONKit.h"
#import "StringTable.h"
#import "BuildingLevel.h"
#import "ObjectConfig.h"
#import "LiftAppDelegate.h"

@implementation SOAPRequest

@synthesize responseData;
@synthesize processId, rType,owner;

- (id)initWithOwner:(id)del {
	if (self = [super init]) {
        self.owner = del;
    }
	return self;
}

- (NSString *) urlencode: (NSString *) url{
    NSArray *escapeChars = [NSArray arrayWithObjects:@";" , @"/" , @"?" , @":" ,
                            @"@" , @"&" , @"=" , @"+" ,
                            @"$" , @"," , @"[" , @"]",
                            @"#", @"!", @"'", @"(", 
                            @")", @"*", nil];
    
    NSArray *replaceChars = [NSArray arrayWithObjects:@"%3B" , @"%2F" , @"%3F" ,
                             @"%3A" , @"%40" , @"%26" ,
                             @"%3D" , @"%2B" , @"%24" ,
                             @"%2C" , @"%5B" , @"%5D", 
                             @"%23", @"%21", @"%27",
                             @"%28", @"%29", @"%2A", nil];
    
    int len = [escapeChars count];
    
    NSMutableString *temp = [url mutableCopy];
    
    int i;
    for(i = 0; i < len; i++)
    {
        
        [temp replaceOccurrencesOfString: [escapeChars objectAtIndex:i]
                              withString:[replaceChars objectAtIndex:i]
                                 options:NSLiteralSearch
                                   range:NSMakeRange(0, [temp length])];
    }
    
    NSString *out = [NSString stringWithString: temp];
    
    return out;
}

- (void) syncAddBuilding:(NSString *)tMobileNo{
    
    rType = REQUEST_TYPE_BUIDLING_REG;
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSString * baseLink = @"http://";
    ObjectConfig * objConf = [delegate.db getConfig];
    baseLink = [baseLink stringByAppendingString:objConf.strBaseLink];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",baseLink,BUILDING_ADD_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"tenant_mobile=%@",tMobileNo];
    responseData = [[NSMutableData alloc] init];
    NSURL * url = [NSURL URLWithString: fullURL];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
        NSLog(@"loading The Key: %@", fullURL);
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	//[[NSURLConnection alloc] initWithRequest:request delegate:self];
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncAccessBuilding:(NSString *)tHashCode{
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    rType = REQUEST_TYPE_BUIDLING_ACCESS;
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSString * baseLink = @"http://";
    ObjectConfig * objConf = [delegate.db getConfig];
    baseLink = [baseLink stringByAppendingString:objConf.strBaseLink];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",baseLink,BUILDING_ACCESS_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"tenant_code=%@",tHashCode];
    responseData = [[NSMutableData alloc] init];
    NSURL * url = [NSURL URLWithString: fullURL];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    NSLog(@"loading The Key: %@", fullURL);
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	//[[NSURLConnection alloc] initWithRequest:request delegate:self];
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncLift:(BuildingLevel *)objBLevel{
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    rType = REQUEST_TYPE_BUIDLING_ACCESS;
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSString * baseLink = @"http://";
    ObjectConfig * objConf = [delegate.db getConfig];
    baseLink = [baseLink stringByAppendingString:objConf.strBaseLink];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",baseLink,BUILDING_LIFT_REQUEST_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"tenant_code=%@&request_source=%d&request_destination=%d&request_type=%@",objBLevel.strHashTenantCode,objBLevel.sourceLevel,objBLevel.destinationLevel,objBLevel.strType];
    NSLog(@"Logging %@ and %@", strParamPlatform, @"test");
    NSLog(@"Destination %d", objBLevel.destinationLevel);
    responseData = [[NSMutableData alloc] init];
    NSURL * url = [NSURL URLWithString: fullURL];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    NSLog(@"loading The Key: %@ and tanent code %@", fullURL,objBLevel.strHashTenantCode);
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	//[[NSURLConnection alloc] initWithRequest:request delegate:self];
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0];
    intResponseStatusCode = 0;
    NSLog(@"Receive String!");
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
    NSLog(@"Did Receive String!");
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	NSLog(@"Did error String! %@",[error localizedDescription]);
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:[error localizedDescription] delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil] show];
    [owner onErrorLoad:processId];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {

	//zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
	NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	responseData = nil;
    
    
   if( rType == REQUEST_TYPE_BUIDLING_REG ){
        NSLog(@"responseString: %@ and process id %d", responseString,processId);
        NSLog(@"status code %d",intResponseStatusCode);
        NSError *error = nil;
        
        NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        if (soapData == nil)
        {
            NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
            abort();
        }
        
        NSMutableDictionary *dictionary;
        JSONDecoder* decoder = [[JSONDecoder alloc]
                                initWithParseOptions:JKParseOptionNone];
        dictionary = [decoder objectWithData:soapData];

        NSLog(@"Feed dictionary count %d",[dictionary count]);
        [owner onJsonLoaded:dictionary withProcessId:processId];
    }
   else if( rType == REQUEST_TYPE_BUIDLING_ACCESS ){
        NSLog(@"responseString: %@ and process id %d", responseString,processId);
        NSLog(@"status code %d",intResponseStatusCode);
        NSError *error = nil;
        
        NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        if (soapData == nil)
        {
            NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
            abort();
        }
        
        NSMutableDictionary *dictionary;
        JSONDecoder* decoder = [[JSONDecoder alloc]
                                initWithParseOptions:JKParseOptionNone];
        dictionary = [decoder objectWithData:soapData];
        
        NSLog(@"Feed dictionary count %d",[dictionary count]);
        [owner onJsonLoaded:dictionary withProcessId:processId];
    }
   else if( rType == REQUEST_TYPE_LIFT ){
       NSLog(@"responseString: %@ and process id %d", responseString,processId);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSMutableDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
    
}

- (void) readResults:(NSMutableDictionary*)dics processId:(NSString *) pid{
	//int proId = [pid intValue];
}

- (void) failedParsing:(NSError*)error processId:(NSString *) pid{
	//[owner onErrorLoad: self.processId];
}


@end

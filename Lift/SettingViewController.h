//
//  SettingViewController.h
//  Lift
//
//  Created by Zayar on 4/3/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController
-(IBAction)onAdd:(id)sender;
- (IBAction)onChangePosition:(UIButton *)sender;
@end

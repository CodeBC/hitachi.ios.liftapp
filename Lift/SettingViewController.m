//
//  SettingViewController.m
//  Lift
//
//  Created by Zayar on 4/3/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "SettingViewController.h"
#import "LiftAppDelegate.h"
#import "Building.h"
#import "Utility.h"
#import "CustomCancel.h"
@interface SettingViewController ()
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UIImageView * imgLogoView;
    IBOutlet UILabel * lblNotice;
    IBOutlet UILabel * lblSelectBuilding;
    IBOutlet UILabel * lblAddBuilding;
    IBOutlet UILabel * lblRemoveBuilding;
    IBOutlet UILabel * lblChangeBuilding;
    IBOutlet UIButton * btnSelectBuilding;
    IBOutlet UIButton * btnAddBuilding;
    IBOutlet UIButton * btnRemoveBuilding;
    IBOutlet UIButton * btnChangeBuilding;
    
    IBOutlet UIImageView * imgBtnLineBg;
    IBOutlet UIButton * btnRemove;
    
}
@property (nonatomic, strong) UIPickerView *uiPickerView;
@property (nonatomic, strong) UIActionSheet *menu;
@property int selectedBuilding;
@property (nonatomic, strong) IBOutlet UIButton * btnDropDown;
@property (nonatomic, strong) NSMutableArray * arrBuilding;
@property (nonatomic, strong) Building * objSelectedBuilding;
@property BOOL isSelected;
@end

@implementation SettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,170,0,0)];
    
    self.uiPickerView.delegate = self;
    self.uiPickerView.showsSelectionIndicator = YES;// note this is default to NO
    
    
    self.menu = [[UIActionSheet alloc] initWithTitle:@"Select a building"
                                       delegate:self
                              cancelButtonTitle:@"Done"
                         destructiveButtonTitle:@"Cancel"
                              otherButtonTitles:nil];
    self.selectedBuilding = 0;
    
    self.arrBuilding = [[NSMutableArray alloc]init];
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    Building * building = [delegate.db getSelectedBuilding];
    if (building) {
        self.isSelected = TRUE;
    }
    else{
        self.isSelected = FALSE;
    }
    
    [self setUpBackgroudViews];
    [self setButtonsViews];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    
    self.navigationItem.title = @"";
}

- (void) setUpBackgroudViews{
    [imgBgView setImage:[UIImage imageNamed:@"bg"]];
    [imgLogoView setImage:[UIImage imageNamed:@"logo"]];
    
    [lblNotice setText:@"In case of fire, do not use lift."];
}

- (void) positionToLeft:(BOOL)left{
    
    int subtract_height = 0;
    int reduce_width = 0;
    int reduce_height = 0;
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        subtract_height = 0;
        reduce_width = 0;
        reduce_height = 0;
    }
    else{
        if ([Utility isLessOSVersion:@"7.0"]) {
            subtract_height = 50;
        }
        else{
            subtract_height = 40;
            reduce_width = 5;
            reduce_height = 2;
        }
        
    }
    
    [self screenviewAdjust];
    
    if (left) {
        [imgBtnLineBg setImage:[UIImage imageNamed:@"bg_btn_line"]];
        /*[imgBtnLineBg setFrame:CGRectMake(0, 0, 0, 0)];
        [lblSelectBuilding setFrame:CGRectMake(0, 0, 0, 0)];
        [lblAddBuilding setFrame:CGRectMake(0, 0, 0, 0)];
        [lblRemoveBuilding setFrame:CGRectMake(0, 0, 0, 0)];
        [lblChangeBuilding setFrame:CGRectMake(0, 0, 0, 0)];
        [btnSelectBuilding setFrame:CGRectMake(0, 0, 0, 0)];
        [btnAddBuilding setFrame:CGRectMake(0, 0, 0, 0)];
        [btnRemoveBuilding setFrame:CGRectMake(0, 0, 0, 0)];
        [btnChangeBuilding setFrame:CGRectMake(0, 0, 0, 0)];*/
        //[imgBtnLineBg setFrame:CGRectMake(192,111,128,386)];
        [imgBtnLineBg setFrame:CGRectMake(0, 0, 0, 0)];
        imgBtnLineBg.alpha = 0.5;
        [lblSelectBuilding setFrame:CGRectMake(110-20,153-subtract_height,123,21)];
        [lblAddBuilding setFrame:CGRectMake(163-120,238-subtract_height,123,21)];
        [lblRemoveBuilding setFrame:CGRectMake(163-120,339-subtract_height,123,21)];
        [lblChangeBuilding setFrame:CGRectMake(108-20, 433-subtract_height,123,21)];
        [btnSelectBuilding setFrame:CGRectMake(21+200,128-subtract_height,70,71)];
        [btnAddBuilding setFrame:CGRectMake(76+100,213-subtract_height,70,71)];
        [btnRemoveBuilding setFrame:CGRectMake(75+100,314-subtract_height,70,71)];
        [btnChangeBuilding setFrame:CGRectMake(20+200,407-subtract_height,70,70)];
        lblSelectBuilding.textAlignment = NSTextAlignmentLeft;
        lblAddBuilding.textAlignment = NSTextAlignmentLeft;
        lblRemoveBuilding.textAlignment = NSTextAlignmentLeft;
        lblChangeBuilding.textAlignment = NSTextAlignmentLeft;
        [btnChangeBuilding setTitle:@"R" forState:normal];
        
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            imgBtnLineBg.alpha = 1;
            [imgBtnLineBg setFrame:CGRectMake(0,111-subtract_height,128,386)];
            [lblSelectBuilding setFrame:CGRectMake(110,153-subtract_height,123,21)];
            [lblAddBuilding setFrame:CGRectMake(163,238-subtract_height,123,21)];
            [lblRemoveBuilding setFrame:CGRectMake(163,339-subtract_height,123,21)];
            [lblChangeBuilding setFrame:CGRectMake(108, 433-subtract_height,123,21)];
            [btnSelectBuilding setFrame:CGRectMake(21,128-subtract_height,70,71)];
            [btnAddBuilding setFrame:CGRectMake(76,213-subtract_height,70,71)];
            [btnRemoveBuilding setFrame:CGRectMake(75,314-subtract_height,70,71)];
            [btnChangeBuilding setFrame:CGRectMake(20,407-subtract_height,70,70)];
        } completion:^(BOOL finished) {
            
        }];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"Position"];
    }
    else{
        [imgBtnLineBg setImage:[UIImage imageNamed:@"bg_btn_line_right"]];
        [imgBtnLineBg setFrame:CGRectMake(320, 0, 0, 0)];
        imgBtnLineBg.alpha = 0.5;
        //[imgBtnLineBg setFrame:CGRectMake(0,111,128,386)];
        [lblSelectBuilding setFrame:CGRectMake(110,153-subtract_height,123,21)];
        [lblAddBuilding setFrame:CGRectMake(163,238-subtract_height,123,21)];
        [lblRemoveBuilding setFrame:CGRectMake(163,339-subtract_height,123,21)];
        [lblChangeBuilding setFrame:CGRectMake(108, 433-subtract_height,123,21)];
        [btnSelectBuilding setFrame:CGRectMake(21,128-subtract_height,70,71)];
        [btnAddBuilding setFrame:CGRectMake(76,213-subtract_height,70,71)];
        [btnRemoveBuilding setFrame:CGRectMake(75,314-subtract_height,70,71)];
        [btnChangeBuilding setFrame:CGRectMake(20,407-subtract_height,70,70)];
        lblSelectBuilding.textAlignment = NSTextAlignmentRight;
        lblAddBuilding.textAlignment = NSTextAlignmentRight;
        lblRemoveBuilding.textAlignment = NSTextAlignmentRight;
        lblChangeBuilding.textAlignment = NSTextAlignmentRight;
        [btnChangeBuilding setTitle:@"L" forState:normal];
        
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            imgBtnLineBg.alpha = 1;
            [imgBtnLineBg setFrame:CGRectMake(192,111-subtract_height,128,386)];
            [lblSelectBuilding setFrame:CGRectMake(110-20,153-subtract_height,123,21)];
            [lblAddBuilding setFrame:CGRectMake(163-120,238-subtract_height,123,21)];
            [lblRemoveBuilding setFrame:CGRectMake(163-120,339-subtract_height,123,21)];
            [lblChangeBuilding setFrame:CGRectMake(108-20, 433-subtract_height,123,21)];
            [btnSelectBuilding setFrame:CGRectMake(21+200,128-subtract_height,70,71)];
            [btnAddBuilding setFrame:CGRectMake(76+100,213-subtract_height,70,71)];
            [btnRemoveBuilding setFrame:CGRectMake(75+100,314-subtract_height,70,71)];
            [btnChangeBuilding setFrame:CGRectMake(20+200,407-subtract_height,70,70)];
        } completion:^(BOOL finished) {
            
        }];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"Position"];
    }
}

- (void) screenviewAdjust{
    int subtract_height = 75;
    
    CGRect lblNoticeFrame = lblNotice.frame;
    NSLog(@"lblNoticeFrame height %f",lblNoticeFrame.origin.y);
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        if ([Utility isLessOSVersion:@"7.0"]) {
            subtract_height = 20;
            lblNoticeFrame.origin.y  = 536 - (subtract_height + 5);
            lblNotice.frame = lblNoticeFrame;
        }
    }
    else{
        if ([Utility isLessOSVersion:@"7.0"]) {
            subtract_height = 100;
            lblNoticeFrame.origin.y  = 536 - (subtract_height + 5);
            lblNotice.frame = lblNoticeFrame;
        }
        else{
            lblNoticeFrame.origin.y  = 536 - (subtract_height + 5);
            lblNotice.frame = lblNoticeFrame;
        }
        
    }
}

- (IBAction)onChangePosition:(UIButton *)sender{
    NSInteger isLeft = [[NSUserDefaults standardUserDefaults] integerForKey:@"Position"];
    
    if (isLeft == 0) {
        [self positionToLeft:YES];
    }
    else if(isLeft == 1){
        [self positionToLeft:NO];
    }
}

- (void) setButtonsViews{
    [imgBtnLineBg setImage:[UIImage imageNamed:@"bg_btn_line"]];
}

- (void)viewWillAppear:(BOOL)animated{
    //[self.navigationController setNavigationBarHidden:YES];
    LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    self.arrBuilding = [delegate.db getAllBuilding];
    
    [delegate disableTheTabs:self.tabBarController];
    Building * building = [delegate.db getSelectedBuilding];
    if (building) {
        CustomCancel *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCancel.frame = CGRectMake(0, 0,60, 30);
        [btnCancel addTarget:self action:@selector(onCancel) forControlEvents:UIControlEventTouchUpInside];
        [btnCancel setTitle:@"Cancel" forState:normal];
        btnCancel.titleLabel.textColor = [UIColor whiteColor];
        
        UIBarButtonItem * btnCan = [[UIBarButtonItem alloc] initWithCustomView:btnCancel];
        btnCan.imageInsets = UIEdgeInsetsMake(0.0, 20, 0, 0);
        self.navigationItem.leftBarButtonItem = btnCan;
    }
    else{
        self.navigationItem.leftBarButtonItem = nil;
    }
    
    #if IS_DEMO
    #else
    
    if (building != nil) {
        int status = [delegate validateBuilding:building];
        
        if (status == 1) {
            self.objSelectedBuilding = building;
            [self.btnDropDown setTitle:building.strName forState:normal];
            self.isSelected = TRUE;
        }
        else if (status == 2 ){
            //message = @"You are not reached the building!";
            //message = @"You are not connected building wifi!";
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info"
                                                           message:@"You must be within premises to use the application."
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
            [delegate.db updateDeselectedBuilding];
            [delegate disableTheTabs:self.tabBarController];
            [self.btnDropDown setTitle:@"Select Building" forState:normal];
            self.isSelected = FALSE;
            //self.tabBarController.selectedIndex = 2;
        }
        else if (status == 3){
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info"
                                                           message:@"Please connect to the building's Wifi before you can proceed."
                                                          delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
            [delegate.db updateDeselectedBuilding];
            [self.btnDropDown setTitle:@"Select Building" forState:normal];
            self.isSelected = FALSE;
            [delegate disableTheTabs:self.tabBarController];
            //self.tabBarController.selectedIndex = 2;
        }
        
        
    }
    else{
        [self.btnDropDown setTitle:@"Select Building" forState:normal];
        self.isSelected = FALSE;
        [delegate.db updateDeselectedBuilding];
        [delegate disableTheTabs:self.tabBarController];
        self.navigationItem.rightBarButtonItem = nil;
    }
    #endif
    
    
    NSInteger isLeft = [[NSUserDefaults standardUserDefaults] integerForKey:@"Position"];
    if (isLeft == 0) {
        [self positionToLeft:NO];
    }
    else if(isLeft == 1){
        [self positionToLeft:YES];
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (pickerView.tag == 0) {
        
        Building * objBuilding = [self.arrBuilding objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);
        return objBuilding.strName;
        
    }
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 0) {
        return [self.arrBuilding count];
    }
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    if (pickerView.tag == 0) {
        self.selectedBuilding = row;
    }
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
    }
    
    else if (buttonIndex == 1) {
        NSLog(@"Other Button Done Clicked and selected index %d",self.selectedBuilding);
        
        if (self.uiPickerView.tag == 0){
            LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            Building * objBuilding = [self.arrBuilding objectAtIndex:self.selectedBuilding];
            self.objSelectedBuilding = objBuilding;
            [self.btnDropDown setTitle:objBuilding.strName forState:normal];
            self.isSelected = TRUE;
            [delegate.db updateDeselectedBuilding];
            [delegate.db updateSelectedBuilding:objBuilding.idx];
            //[self performSegueWithIdentifier:@"toTenant" sender:nil];
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }
    }
}

- (IBAction)onRemove:(id)sender{
    if (!self.isSelected) {
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info"
                                                       message:@"Please select a building that you would like to remove."
                                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else {
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Warning"
                                                       message:@"Are you sure you want to remove the building?"
                                                      delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
        alert.tag= 1;
        [alert show];
    }
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex{
    
    
    if([alertView tag]==1){
       if(buttonIndex == 1) {
           LiftAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
           [delegate.db deleteBuildingBy:self.objSelectedBuilding.idx];
           [self.arrBuilding removeObjectAtIndex:self.selectedBuilding];
           [self.btnDropDown setTitle:@"Select Building" forState:normal];
           [delegate.db updateDeselectedBuilding];
           self.isSelected = FALSE;
           [delegate disableTheTabs:self.tabBarController];
		}
    }
}

- (IBAction)onAdd:(id)sender{
    if ([self.arrBuilding count]>0) {
        [self.menu setTitle:@"Select a building"];
        //UIButton * btn = (UIButton *)sender;
        // Add the picker
        self.uiPickerView.tag = 0;
        self.uiPickerView.delegate = self;
        [self.uiPickerView reloadAllComponents];
        [self.uiPickerView selectRow:self.selectedBuilding inComponent:0 animated:YES];
        [self.menu addSubview:self.uiPickerView];
        [self.menu showInView:self.view];
        [self.menu setBounds:CGRectMake(0,0,320,600)];
    }
    else {
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info"
                                                       message:@"Please add a building before you can proceed."
                                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onCancel{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

@end

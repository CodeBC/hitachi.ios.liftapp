//
//  StringTable.h
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface StringTable : NSObject {
    
}

extern NSString * const DBNAME;
extern NSString * const APP_TITLE;
extern NSString * const APP_ID;

extern NSString * const CONFIG_LINK;
extern NSString * const BASE_LINK;
extern NSString * const BUILDING_ADD_LINK;
extern NSString * const BUILDING_ACCESS_LINK;
extern NSString * const BUILDING_LIFT_REQUEST_LINK;

extern NSString * const VISSITOR_TYPE;
extern NSString * const MOBILE_T_TYPE;

extern NSString * const ANSWER_STATUS_PENDING;
extern NSString * const ANSWER_STATUS_ANSWERED;
extern NSString * const ANSWER_STATUS_REJECTED;

extern NSString * const APN_SERVER_PATH;

extern int const STATUS_ACTION_SUCCESS;
extern int const STATUS_RETURN_RECORD;
extern int const STATUS_ACTION_FAILED;
extern int const STATUS_SESSION_EXPIRED;
extern int const STATUS_NO_RECORD_FOUND;

extern double const CACHE_DURATION;

extern NSString * const GROUP_CALL_STR;

@end

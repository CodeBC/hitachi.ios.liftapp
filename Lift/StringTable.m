//
//  StringTable.m
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "StringTable.h"


@implementation StringTable

NSString * const DBNAME	= @"lift.sqlite";
NSString * const APP_TITLE = @"Lift";
NSString * const APP_ID = @"---";
NSString * const BASE_LINK= @"http://www.iwh.com.sg/sl/";
NSString * const GROUP_CALL_STR = @"1,2,3,4,5,6,7,8";
NSString * const BUILDING_ADD_LINK= @"modules/external/_ext_register_tenant.php";
NSString * const BUILDING_ACCESS_LINK= @"modules/external/_ext_get_tenant_level.php";
NSString * const BUILDING_LIFT_REQUEST_LINK= @"modules/external/_ext_request_lift.php";

NSString * const VISSITOR_TYPE = @"V";
NSString * const MOBILE_T_TYPE = @"M";


NSString * const APN_SERVER_PATH  = @"test.balancedconsultancy.com.sg/tsmaths/apn";

int const STATUS_ACTION_SUCCESS = 1;
int const STATUS_RETURN_RECORD = 2;
int const STATUS_ACTION_FAILED = 3;
int const STATUS_SESSION_EXPIRED = 5;
int const STATUS_NO_RECORD_FOUND = 6;

NSString * const ANSWER_STATUS_PENDING = @"P";
NSString * const ANSWER_STATUS_ANSWERED = @"A";
NSString * const ANSWER_STATUS_REJECTED = @"R";

double const CACHE_DURATION		= 86400 * 5.0;
@end

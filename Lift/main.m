//
//  main.m
//  Lift
//
//  Created by Zayar on 4/2/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LiftAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LiftAppDelegate class]));
    }
}
